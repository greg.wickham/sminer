/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "db-table.h"
#include    "db-field.h"
#include    "stats.h"
#include    "query.h"
#include    "mytypes.h"
#include    "timer.h"
#include    "query.h"
#include    "postgres.h"
#include    "app.h"

#include <postgres_ext.h>

#undef NO_DELETE

namespace DB {

#define INDEX_SEPARATOR "|"

    Table_map_t Table::tables;

    Table::Table( const char *type, std::string &name, bool preload ) : _stats( type ) {
        _type = xstrdup( type );
        _pg_table_name = xstrdup( name.c_str() );
        _pg_table_name_len = xstrlen( _pg_table_name );
        _fields_count = 0;
        _fields = NULL;
        _preload = preload;
        _indexes = NULL;
        _indexes_count = 0;
        _pkey = -1;
        _debug = false;
        _last = NULL;
        _lock = false;
        _row_count = 0;
        _parent = NULL;
        _pfindex = -1;
        _lfindex = -1;
    }

    bool Table::lock( void ) {
        return( _lock );
    }

    void Table::lock( bool value ) {
        _lock = value;
    }

    Row *Table::last( void ) {
        return( _last );
    }

    void Table::name( const char *text ) {
        _pg_table_name = xstrdup( text );
    }

    const char *Table::name( void ) {
        return( _pg_table_name );
    };

    const char *Table::type( void ) {
        return( _type );
    };

    const char *Table::table_name( void ) {
        return( _pg_table_name );
    }

    std::string yamlNodeScalar( const YAML::Node &node, const char *key ) {

        YAML::Node element;

        try {
            element = node[ key ];
        } catch ( const YAML::InvalidNode &e ) {
            throw( AppExcept("key '%s' is not present", key ) );
        }

        if ( element.Type() == YAML::NodeType::Null ) {
            throw( AppExcept("'%s' is not defined", key ) );
        } else if ( element.Type() != YAML::NodeType::Scalar ) {
            throw( AppExcept("'%s' expected Scalar" ) );
        } else {
            return( element.as<std::string>() );
        }

    }

    void Table::configure( YAML::Node data ) {

        static const char *valid_tables[] = {
            "job",
            "step",
            "node",
            "user",
            "cluster",
            "launch",
            "submit",
            "step_node_link",
            "job_node_link",
            NULL
        };

        if ( data.size() == 0 ) {
            throw( AppExcept("No configuration data" ) );
        }

        /* define the tables */

        int index = 0;

        for ( YAML::const_iterator i = data.begin() ; i != data.end() ; i ++, index ++ ) {

            std::string tmp =  i->first.as<std::string>().c_str();
            const char *tableType = tmp.c_str();

            bool match = false;

            for ( int ctr = 0 ; valid_tables[ ctr ] ; ctr ++ ) {
                if ( strcasecmp( valid_tables[ ctr ], tableType ) == 0 ) {
                    match = true;
                    break;
                }
            }

            if ( ! match ) {
                app.alert( "table type '%s' is not known - skipping", tableType );
                continue;
            }

            std::string tableName;

            try {
                tableName = yamlNodeScalar( i->second, "name" );
            } catch ( AppExcept &e ) {
                throw( AppExcept("Table type '%s': 'name' must be a Scalar", tableType ) );
            }

            bool preload = yamlNodeBoolean( i->second, "preload", false );

            YAML::Node fields = i->second["fields"];

            if ( fields.Type() != YAML::NodeType::Map ) {
                throw( AppExcept("Table type '%s' missing field definitions", tableType ) );
            }

            Table_map_i  j;

            if (( j = tables.find( tableType )) != tables.end() ) {
                throw( AppExcept("Table type '%s' already defined", tableType ) );
            }

            Table *instance = new Table( tableType, tableName, preload );
            try {
                instance->build( fields );
                instance->indexes( i->second["index"] );
                instance->describe();
            } catch ( AppExcept &e ) {
                throw( AppExcept( "table %s(%s): %s", tableType, tableName.c_str(), e.what() ) );
            }

            tables.insert( Table_pair_t( tableType, instance ));

        }

    }

    void Table::indexes( YAML::Node node ) {
        
        if ( node.Type() == YAML::NodeType::Scalar ) {
            indexAdd( node );
        } else if ( node.Type() == YAML::NodeType::Sequence ) {
            for ( YAML::const_iterator i = node.begin() ; i != node.end() ; i ++) {
                indexAdd( *i );
            }
        }

    }

    void Table::indexAdd( const YAML::Node &node ) {

        const char *field = node.as<std::string>().c_str();

        for ( findex_t index = 0 ; index < _fields_count ; index ++ ) {

            if ( strcasecmp( _fields[ index ]->name(), field ) == 0 ) {

                int number = _indexes_count;

                _indexes_count ++;

                _indexes = (findex_t*)realloc( _indexes, sizeof( findex_t ) * _indexes_count );
                _indexes[ number ] = index;

                return;
            }

        }

        throw( AppExcept("index field '%s' is not found", field ) );
    }

    Table *Table::fetch( const char *table_type ) {

        Table_map_i  j;

        if (( j = tables.find( table_type )) != tables.end() ) {
            return( j->second );
        } else {
            return( NULL );
        }

    }

    Table *Table::fetchByName( const char *name ) {

        for ( Table_map_i i = tables.begin() ; i != tables.end() ; i ++ ) {
            if ( strcasecmp( name, i->second->_pg_table_name ) == 0 ) {
                return( i->second );
            }
        }

        return( NULL );
    }
    
    Table *Table::fetchByName( const char *name, size_t len ) {

        for ( Table_map_i i = tables.begin() ; i != tables.end() ; i ++ ) {
            if ( len != i->second->_pg_table_name_len ) {
                continue;
            } else if ( strncasecmp( name, i->second->_pg_table_name, len ) == 0 ) {
                return( i->second );
            }
        }

        return( NULL );
    }

    void Table::build( YAML::Node &fields ) {

        int index = 0;

        for ( YAML::const_iterator i = fields.begin() ; i != fields.end() ; i ++, index ++ ) {

            std::string name = i->first.as<std::string>();

            for ( findex_t index = 0 ; index < _fields_count ; index ++ ) {
                if (( _fields[ index ] != NULL )&&( strcasecmp( name, _fields[ index ]->name() ) == 0 )) {
                    app.error("name already used %s", name );
                    /* duplicate field names not permitted */
                    throw( AppExcept( "table name already in use" ) );
                }
            }

            /* a simple look to check the defined values */

            char *parser = NULL;
            char *format = NULL;
            char *slurm = NULL;
            char *type = NULL;
            char *references = NULL;
            char *calc = NULL;
            char *default_value = NULL;
            char *trackable = NULL;

            static struct {
                const char  *name;
                char **ptr;
            } types[] = {
                { "slurm", &slurm },
                { "parser", &parser },
                { "format", &format },
                { "type", &type },
                { "references", &references },
                { "trackable", &trackable },
                { "default", &default_value },
                { "calc", &calc },
                { NULL, NULL }
            };

            /* check whether any invalid attributes are referenced */

            for ( YAML::const_iterator j = i->second.begin() ; j != i->second.end() ; j ++ ) {

                std::string key = j->first.as<std::string>();

                bool found = false;

                for ( int index = 0 ; types[ index ].name ; index ++ ) {
                    if ( key == types[ index ].name ) {
                        found = true;
                    }
                }

                if ( ! found ) {
                    throw( AppExcept( "field %s attribute '%s' is not valid", name, key.c_str() ));
                }

            }

            /* process the valid attributes */

            for ( int index = 0 ; types[ index ].name ; index ++ ) {

                YAML::Node node;

                try {
                    node = i->second[ types[ index ].name ];
                } catch ( const YAML::InvalidNode &e ) {
                    continue;
                }

                if ( node.Type() == YAML::NodeType::Scalar ) {
                    *( types[ index ].ptr ) = xstrdup( node.as<std::string>().c_str() );
                }
            }

            _fields_count ++;
            _fields = (Field**)realloc( _fields, sizeof( Field* ) * _fields_count );

            _fields[ _fields_count - 1 ] = Field::make(
                    this,
                    name.c_str(),
                    parser,
                    format,
                    slurm,
                    references, default_value,
                    type, calc, trackable );

            if ( parser != NULL ) {

                if ( xstrcasecmp( parser, "pkey" ) == 0 ) {
                    _pkey = _fields_count - 1;
                } else {
                    throw( AppExcept( "field %s, parser '%s' is not valid",
                        name.c_str(), parser ) );
                }

            }

            for ( int index = 0 ; types[ index ].name ; index ++ ) {
                mfree( *types[ index ].ptr );
            }

        }

        /* allocate trampoline storage for values collected from the Field objects */
        _values = (Value_t*)calloc( sizeof( Value_t ), _fields_count );

        if ( app.verbose ) {
            app.info("    Table %s (type %s) has %ld fields configured", _pg_table_name, _type, _fields_count );
        }

    }

    void Table::constructIndex( std::string &index, Row *row ) {
        for ( int ctr = 0 ; ctr < _indexes_count ; ctr ++ ) {
            if ( ctr > 0 ) {
                index += INDEX_SEPARATOR;
            }
            index += row->fetch( _indexes[ ctr ] );
        }
    }

#if 0
    void Table::constructIndexFromSlurm( std::string &index, const char *(Field::*cb)( void ) ) {

        //printf("constructIndexFromSlurm ( %s )\n", _pg_table_name );

        for ( int ctr = 0 ; ctr < _indexes_count ; ctr ++ ) {
            if ( ctr > 0 ) {
                index += INDEX_SEPARATOR;
            }

            //printf("index %d -> %d\n", ctr, _indexes[ ctr ].index );
            //printf("\t  key = %s\n", _fields[ _indexes[ ctr ].index  ]->name() );
            //printf("\tvalue = %s\n", (_fields[ _indexes[ ctr ].index  ]->*cb)() );

            index += (_fields[ _indexes[ ctr ].index  ]->*cb)();

        }
    }
#endif

    void Table::preload( const char *time_start, const char *time_end, Table *parent ) {

        _parent = parent;

        Query query;

        if (( strcasecmp( "job", _type ) == 0 )||( strcasecmp( "step", _type ) == 0 )) {

            query.msql("SELECT * FROM %s WHERE", _pg_table_name );

        } else if ( !  makeJoinedQuery( query ) ) {

            throw( AppExcept( "Table %s is not referenced by another table - preloading denied", _pg_table_name ));
        }

        query.sql += " time_start < $1::TIMESTAMP WITHOUT TIME ZONE AND"
            " time_start IS NOT NULL AND"
            " COALESCE( time_end >= $2::TIMESTAMP WITHOUT TIME ZONE, time_end IS NULL )";
         // AND NOT ( time_start = time_end AND time_start IS NOT NULL AND time_end IS NOT NULL )";
        query.parameter( time_end );
        query.parameter( time_start );

        /* identify indexes */

        if ( _parent ) {

            for ( int ctr = 0 ; ctr < _indexes_count ; ctr ++ ) {

                if (( _pfindex = _parent->findField( _fields[ _indexes[ ctr ] ]->name() )) >= 0 ) {
                    _lfindex = ctr;
                    break;
                }

            }

        }

        preloadRunQuery( query );

    }

    void Table::preload( void ) {

        Query query;

        /* load all records */
        query.msql("SELECT * FROM %s", _pg_table_name );

        preloadRunQuery( query );

    }

    void Table::preloadRunQuery( Query &query ) {

        Timer timer;

        timer.start();

        if ( app.debug( DEBUG_PRELOAD ) ) {
            query.dump( __FILE__, __LINE__ );
        }

        if ( pg.execute( query ) < 0 ) {
            throw( AppExcept( "failed to select rows from %s: %s", _pg_table_name, pg.errorMessage() ) );
        } else if ( pg.resultStatus() != PGRES_TUPLES_OK ) {
            query.dump( __FILE__, __LINE__ );
#warning FIXME: postgres error may span more than one line
            app.error("PG %s", pg.errorMessage() );
            throw( AppExcept( "query '%s' failed to return tuples (error code %d)",
                        _pg_table_name, pg.resultStatus() ) );
        }

        int rows;

        if (( rows = pg.ntuples() ) < 0 ) {
            throw( AppExcept( "%s: pg.ntuples() < 0", _pg_table_name ) );
        }

        /* create the field mapping */

        int nfields = pg.nfields();

        AutoIntPtr mapping( nfields );

        int pgIndexNum = -1;

        for ( int ctr = 0 ; ctr < nfields ; ctr ++ ) {

            mapping.data[ ctr ] = -1; /* not in use */

            const char *fname = pg.fname( ctr );

            zdebug( _debug, "field %d = %s\n", ctr, fname );

            if (( _pfindex >= 0 )&&( strcmp( fname, _fields[ _lfindex ]->name() ) == 0 )) {
                pgIndexNum = ctr;
            }

            findex_t match = 0;

            for ( match = 0 ; match < _fields_count ; match ++ ) {
                if ( strcasecmp( _fields[ match ]->name(), fname ) == 0 ) {
                    break;
                }
            }

            if ( match == _fields_count ) {
                /* not found, so ignore this field */
                continue;
            }

#ifdef DEBUG
            printf("[%d] = %s to %ld\n", ctr, fname, match );
#endif

            mapping.data[ ctr ] = match; /* in use */


        }

        /* process each returned row */

        for ( int row_index = 0 ; row_index < rows ; row_index ++ ) {

            /* create a new row */

            Row *row = new Row( this, _fields_count );

            /* process columns of each row */

            for ( int column_index = 0 ; column_index < nfields ; column_index ++ ) {

                if ( mapping.data[ column_index ] >= 0 ) {

#if 0
                    zdebug( _debug, "X1 injection %s[%d] to %d with %s\n",
                            pg.fname( column_index ),
                            column_index,
                            mapping.data[ column_index ],
                            pg.getvalue( row_index, column_index )
                          );
#endif

                    if ( pg.getisnull( row_index, column_index ) ) {
                        row->set( mapping.data[ column_index ], NULL );
                    } else {

                        zdebug( _debug,
                                "row set %s=%s\n",
                                pg.fname( column_index ),
                                pg.getvalue( row_index, column_index ) );

                        row->set( mapping.data[ column_index ], pg.getvalue( row_index, column_index ) );
                    }

                }

            }

            /* connect to parent, if the parent exists */

            if ( _parent && ( pgIndexNum >= 0 ) ) {
                /* link to parent table */

                Row *pRow;

                if (( pRow = _parent->getByPrimaryIndex( row->fetch( _lfindex ))) != NULL ) {
                    row->parent( pRow );
                }

            }

            /* save the row using the index */

            std::string index;

            constructIndex( index, row );

            _rows.insert( Row_pair_t( index, row ));

            /* maintain the primary key row dict */
            if ( _pkey >= 0 ) {
                const char *pkey = row->fetch( _pkey );
                _rows_by_pkey.insert( Row_pair_t( pkey, row ));
            }

        }

        timer.stop();

        app.info("loaded %6ld records from %-27s in %11.3lf seconds (%9.2lf rps)",
                _rows.size(),
                _pg_table_name,
                timer.seconds(),
                (double)_rows.size() / timer.seconds() );

    }

    void Table::update( Row *row ) {

        zdebug( _debug, "update (table %s)\n", _pg_table_name );

        Query query;

        query.msql("UPDATE %s SET ", _pg_table_name );

        int pnumber = 1;

        for ( findex_t ctr = 0 ; ctr < _fields_count ; ctr ++ ) {

            zdebug( _debug, "field[%d] %s value %s\n", ctr, _fields[ ctr ]->name(), row->fetch( ctr ) );

            if ( ! row->isset( ctr ) ) {
                /* only include fields that have defined values */
                continue;
            }

            if ( row->dirty( ctr ) ) {

                //zdebug( _debug, "field[%d] %s is dirty value %s (was %s)\n", ctr, _fields[ ctr ]->name(), row->fetch( ctr ), row->db( ctr ) );

                if ( pnumber > 1 ) {
                    query.sql += ", ";
                }

                const char *value = row->fetch( ctr );

#if 0
                /* FIXME - use of _pkey will break tables with compound keys */
                if ( app.updates )  {
                    app.info("%s = %s : %s to %s",
                            _fields[ _pkey ]->name(), row->fetch( _pkey ),
                            _fields[ ctr ]->name(), value );
                    /* row->dump( _pg_table_name, _fields ); */
                }
#endif

                if ( value ) {
                    char *tmp = mprintf("%s = $%d::%s", _fields[ ctr ]->name(), pnumber, _fields[ ctr ]->cast() );
                    query.sql += tmp;
                    mfree( tmp );
                    query.parameter( row->fetch( ctr ) );
                    pnumber ++;
                } else {
                    char *tmp = mprintf("%s = NULL", _fields[ ctr ]->name() );
                    query.sql += tmp;
                    mfree( tmp );
                }

            }

        }

        if ( pnumber == 1 ) {
            throw( AppExcept("row was marked as dirty but no fields were dirty" ) );
        }

        /* construct the "where" clause for the update */

        query.sql += " WHERE";

        for ( int i = 0 ; i < _indexes_count ; i ++ ) {

            if ( i > 0 ) {
                query.sql += " AND";
            }

            int findex = _indexes[ i ];

            char *tmp = mprintf(" %s = $%d::%s", _fields[ findex ]->name(), pnumber ++, _fields[ findex ]->cast() );
            query.sql += tmp;
            mfree( tmp );

        }

        /* and add the parameters for the primary key */

        for ( int i = 0 ; i < _indexes_count ; i ++ ) {
            query.parameter( row->fetch( _indexes[ i ] ) );
        }

        if ( app.debug( DEBUG_UPDATE ) ) {
            query.dump( __FILE__, __LINE__ );
        }

        _stats.update ++;
        if ( pg.execute( query ) < 0 ) {
            throw( AppExcept( "failed to select rows from %s: %s", _pg_table_name, pg.errorMessage() ) );
        } else if ( pg.resultStatus() == PGRES_COMMAND_OK ) {

            /* ok - success */
            row->sync();

        } else if ( pg.resultStatus() != PGRES_TUPLES_OK ) {

            if ( strcmp( "42703", pg.resultErrorField( PG_DIAG_SQLSTATE ) ) == 0 ) {
                /* 42703 = column does not exist */

                throw( AppFatal( "(%s) %s",
                        pg.resultErrorField( PG_DIAG_SQLSTATE ),
                        pg.resultErrorField( PG_DIAG_MESSAGE_PRIMARY ) ) );

            } else {

                query.dump( __FILE__, __LINE__ );

                throw( AppFatal( "%s UPDATE (%s) %s",
                            _pg_table_name,
                            pg.resultErrorField( PG_DIAG_SQLSTATE ),
                            pg.resultErrorField( PG_DIAG_MESSAGE_PRIMARY ) ) );

            }

        } else {
            throw( AppExcept( "%s: unexpected result code %d",
                        _pg_table_name, pg.resultStatus() ) );
        }

    }

    void Table::insert( Row *row ) {

        zdebug( _debug, "insert()\n");

        Query query;

        query.msql("INSERT INTO %s (", _pg_table_name );

        int pnumber = 1;

        std::string ins;

        for ( findex_t index = 0 ; index < _fields_count ; index ++ ) {

            const char *value;

            if ( ! row->isset( index ) ) {
                /* only include rows that have valid values */
                continue;
            }
  
            if (( value = row->fetch( index )) == NULL ) {
                /* skipping empty values */
                continue;
            }

            /* first, the SQL insert string */

            if ( pnumber > 1 ) {
                query.sql += ", ";
            }

            query.sql += _fields[ index ]->name();

            /* parameters for the insert string */

            if ( pnumber > 1 ) {
                ins += ", ";
            }
                
            char *tmp = mprintf("$%d::%s", pnumber, _fields[ index ]->cast() );
            ins += tmp;
            mfree( tmp );

            query.parameter( value );
#ifdef DEBUG
            printf("%s = %s\n", _fields[ index ]->name(), value );
#endif
            pnumber ++;
        }

        query.sql += " ) VALUES ( " + ins + " )";

        if ( _pkey >= 0 ) {
            query.sql += " RETURNING ";
            query.sql += _fields[ _pkey ]->name();
        }

        /* was 'job_index' */

        if ( app.debug( DEBUG_INSERT ) ) {
            query.dump(__FILE__, __LINE__ );
        }

tryAgain:

        if ( pg.execute( query ) < 0 ) {
            throw( AppExcept( "failed to select rows from %s: %s", _pg_table_name, pg.errorMessage() ) );
        }

        zdebug( _debug, "pg.resultStatus() = %d\n", pg.resultStatus() );

        switch ( pg.resultStatus() ) {

            int rows;

            case PGRES_COMMAND_OK:

                _stats.insert ++;
                row->sync();
                zdebug(_debug, "INSERT PGRES_COMMAND_OK\n");
                break;

           default:

                if ( pg.errorContains( "duplicate" ) ) {

                    /* duplicate insert error
                     *
                     * When this occurs, fetch the record
                     * from the DB and then perform an update
                     * if necessary
                     */

                    zdebug( _debug,"duplicate detected\n");

#if 0
                    printf("duplicate detected\n");
                    query.dump( __FILE__, __LINE__ );
                    exit( 1 );
#endif

                    _stats.duplicate ++;

                    try {
                       select( row );
                    } catch( AppExcept &e ) {
                        /* row failed to load */
                        throw( AppExcept("%s: row insert: %s", _pg_table_name, e.what() ) );
                    }

                } else if ( strcmp( "23503", pg.resultErrorField( PG_DIAG_SQLSTATE ) ) == 0 ) {
                    /* 23503 = FOREIGN KEY VIOLATION */

                    const char *detail;
                    
                    if (( detail = pg.resultErrorField( PG_DIAG_MESSAGE_DETAIL )) == NULL ) {
                        app.error( "%s: %s", _pg_table_name, pg.errorMessage() );
                        throw( AppExcept( "%s: foriegn key error but PG_DIAG_MESSAGE_DETAIL is NULL", _pg_table_name ) );
                    }

                    const char *ptr = detail;

                    app.info("%s: insert (%s): %s", _pg_table_name,
                            pg.resultErrorField( PG_DIAG_SQLSTATE ),
                            detail );

                    while (( *ptr )&&( *ptr != '(' )) {
                        ptr++;
                    }

                    if ( *ptr != '(' ) {
                        throw( AppExcept( "%s: badly formatted detail message: %s",
                                    _pg_table_name, detail ) );
                    }

                    ptr ++;
                    
                    size_t len = 0;

                    for ( len = 0 ; ptr[ len ] ; len ++ ) {
                        if ( ptr[ len ] == ')' ) {
                            break;
                        }
                    }

                    if ( ! ptr[ len ] ) {
                        throw( AppExcept( "%s: badly formatted detail message: %s",
                                    _pg_table_name, detail ) );
                    }

                    int findex;

                    if (( findex = findField( ptr, len )) < 0 ) {
                        std::string tmp( ptr, len );
                        throw( AppExcept( "%s: field '%s' identifed by detail message is not valid: %s",
                                    _pg_table_name, tmp.c_str(), detail ) );
                    }

                    /* now ensure a host record is created for the other table */
                    Table       *ref_table;
                    findex_t    ref_index;

                    if (( ref_table = _fields[ findex ]->references( &ref_index )) == NULL ) {

                        std::string tmp( ptr, len );

                        throw( AppExcept( "%s: foreign key error for %s(%s) however no reference for other table",
                                    _pg_table_name, _fields[ findex ]->name(), tmp.c_str() ));

                    }

                    ref_table->insertByKey( ref_index, _values[ findex ].value );

                    goto tryAgain;

                } else {

                    if ( strcmp( "22003", pg.resultErrorField( PG_DIAG_SQLSTATE ) ) == 0 ) {

                        /* 22003 numeric overflow */
                        throw( AppWarning("table %s: INSERT failed: (CODE %s) %s", _pg_table_name,
                                pg.resultErrorField( PG_DIAG_SQLSTATE ),
                                pg.errorMessage() ) );

                    } else {

                        query.dump( __FILE__, __LINE__ );
                        throw( AppExcept("table %s: INSERT failed: (CODE %s) %s", _pg_table_name,
                                pg.resultErrorField( PG_DIAG_SQLSTATE ),
                                pg.errorMessage() ) );

                    }
                }

                break;

            case PGRES_TUPLES_OK:

                zdebug( _debug, "%s: PGRES_TUPLES_OK pg.ntuples() = %d\n", _pg_table_name, pg.ntuples() );

                _stats.insert ++;

                if (( rows = pg.ntuples() ) != 1 ) {
                    query.dump( __FILE__, __LINE__ );
                    throw( AppExcept( "%s: INSERT expected exactly one row (found %d)",
                                _pg_table_name, pg.ntuples() ) );
                }

                row->update( _pkey, pg.getvalue( 0, 0 ) );
                row->sync();

                break;
        }

    }

    void Table::build( Query &query, Value_t *values ) {

        zdebug( _debug, "building query to load record from %s with:\n", _pg_table_name );

        query.msql("SELECT * FROM %s WHERE ", _pg_table_name );

        for ( int ctr = 0 ; ctr < _indexes_count ; ctr ++ ) {

            if ( ctr > 0 ) {
                query.sql += " AND ";
            }

            int findex = _indexes[ ctr ];

            zdebug( _debug, "  %s = '%s'::%s\n", _fields[ findex ]->name(), values[ findex ], _fields[ findex ]->cast() );

            char *tmp = mprintf( "%s = $%d::%s", _fields[ findex ]->name(), ctr + 1, _fields[ findex ]->cast() );
            query.sql += tmp;
            mfree( tmp );

            if ( values[ findex ].value == NULL ) {
                app.error("%s(%s) is NULL for index", _pg_table_name, _fields[ findex ]->name() );
                assert( 0 );
            }

            query.parameter( values[ findex ].value );

        }

    }

    /*
     * FUNCTION:    select
     *
     * PARAMS:      Row * - contains keys for loading
     *
     * RETURNS:     bool    true - row loaded ok
     *                      false - row not loaded
     *
     * THROWS:      exception on any DB error
     *
     * WHATITDOES:  "selects" the row from the database
     *              for merging with incore storage.
     *              Mostly it is expected this is for
     *              fetching the primary key (needed for
     *              table joins)
     *
     * FIXME:       The loaded row maybe different.
     *              Currently an exception is thrown,
     *              but needs to be modified to update.
     */
    void Table::select( Row *row ) {

        zdebug( _debug, "%s: select()\n", _pg_table_name );

        Query   query;

        query.msql("SELECT * FROM %s WHERE ", _pg_table_name );

        /* always select individual records based on the indexes */

        for ( findex_t ctr = 0 ; ctr < _indexes_count ; ctr ++ ) {

            findex_t    findex = _indexes[ ctr ];
            const char  *value;

            /* mandatory: a value must be set */
            if ( ! row->isset( findex ) ) {

                throw( AppExcept( "%s: field %s required for index but is not set",
                            _pg_table_name, _fields[ findex ]->name() ) );

            } else if (( value = row->fetch( findex )) == NULL ) {

                throw( AppExcept( "%s: field %s required for index has NULL value",
                            _pg_table_name, _fields[ findex ]->name() ) );

            } else if ( ctr > 0 ) {

                query.sql += " AND ";

            }
            
            char *tmp = mprintf( "%s = $%d::%s", _fields[ findex ]->name(), ctr + 1, _fields[ findex ]->cast() );
            query.sql += tmp;
            mfree( tmp );

            query.parameter( value );

        }

        if ( app.debug( DEBUG_SELECT ) ) {
            query.dump( __FILE__, __LINE__ );
        }

        if ( pg.execute( query ) < 0 ) {
            throw( AppExcept( "failed to select row from %s: %s", _pg_table_name, pg.errorMessage() ) );
        } else if ( pg.resultStatus() != PGRES_TUPLES_OK ) {
            throw( AppExcept( "%s: failed to return tuples (error code %d)",
                        _pg_table_name, pg.resultStatus() ) );
        }

        /* 2. build the PG field order to index mapping */

        if ( pg.ntuples() == 0 ) {

            throw( AppExcept( "%s: select with unique key returned no rows" ) );

        } else if ( pg.ntuples() != 1 ) {

            throw( AppExcept( "%s: select with unique key returned multiple (%d) rows", pg.ntuples() ) );

        }

        /* 3. merge the returned data into the row */

        for ( int index = 0 ; index < pg.nfields() ; index ++ ) {

            for ( findex_t ctr = 0 ; ctr < _fields_count ; ctr ++ ) {

                if ( strcmp( _fields[ ctr ]->name(), pg.fname( index ) ) == 0 ) {
                    row->set( ctr, pg.getisnull( 0, index ) ? NULL : pg.getvalue( 0, index ) );
                    break;
                }

            }

        }

        if ( row->dirty() ) {

            try {
                this->update( row );
            } catch ( AppExcept &e ) {
                throw( AppExcept( "%s: update dirty row: %s", _pg_table_name, e.what() ) );
            }

        }

    }

    void Table::select( Query &query ) {

        /* 0. dump the query */

        //query.dump();

        /* 1. execute the query */

        _stats.select ++;
        if ( pg.execute( query ) < 0 ) {
            throw( AppExcept( "failed to select rows from %s: %s", _pg_table_name, pg.errorMessage() ) );
        } else if ( pg.resultStatus() != PGRES_TUPLES_OK ) {
            throw( AppExcept( "%s: failed to return tuples (error code %d)",
                        _pg_table_name, pg.resultStatus() ) );
        }

        /* 2. build the PG field order to index mapping */

        int nfields = pg.nfields();

        AutoIntPtr mapping( nfields );
        AutoIntPtr revMap( nfields );

        for ( int ctr = 0 ; ctr < nfields ; ctr ++ ) {

            mapping.data[ ctr ] = -1; /* not in use */

            const char *fname = pg.fname( ctr );

            findex_t match = 0;

            for ( match = 0 ; match < _fields_count ; match ++ ) {
                if ( strcasecmp( _fields[ match ]->name(), fname ) == 0 ) {
                    break;
                }
            }

            if ( match == _fields_count ) {
                /* not found, so ignore this field */
#ifdef DEBUG
                printf("[-] %s not found\n", fname );
#endif
                continue;
            }

#ifdef DEBUG
            printf("[%d] = %s to %ld\n", ctr, fname, match );
#endif
            revMap.data[ match ] = ctr;

            mapping.data[ ctr ] = match; /* in use */

        }

        /* 3. Lookup over each row */

        for ( int row_index = 0 ; row_index < pg.ntuples() ; row_index ++ ) {

            /* 3. Construct an index */

            std::string index;

            for ( int ctr = 0 ; ctr < _indexes_count ; ctr ++ ) {
                if ( ctr > 0 ) {
                    index += "|";
                }

                index += pg.getvalue( row_index, revMap.data[ _indexes[ ctr ] ] );
                
            }

            Row *row = NULL;

            Row_map_i i;

            if (( i = _rows.find( index )) != _rows.end() ) {
#if 0
                zdebug("row %s already exists\n", index.c_str() );
#endif
                row = i->second;
            } else {
#if 0
                zdebug("row %s created\n", index.c_str() );
#endif
                row = new Row( this, _fields_count );
                _rows.insert( Row_pair_t( index, row ));
            }

            /* save the data */

            for ( int column_index = 0 ; column_index < nfields ; column_index ++ ) {

                if ( mapping.data[ column_index ] >= 0 ) {

#if 0
                    zdebug( _debug, "X2 injection %s[%d] to %d with %s\n",
                            pg.fname( column_index ),
                            column_index,
                            mapping.data[ column_index ],
                            pg.getvalue( row_index, column_index )
                          );
#endif

                    if ( pg.getisnull( row_index, column_index ) ) {
                        row->set( mapping.data[ column_index ], NULL );
                    } else {
                        row->set( mapping.data[ column_index ], pg.getvalue( row_index, column_index ) );
                    }
                    

                }

            }

        }

    }

    void Table::stats( void ) {

        if ( app.nodelete() ) {

            app.info("%-14s %-27s %7s %7s %7s %7s %7s %7s %7s",
                "Type", "Table Name", "Ins", "Dup", "Clean", "Update", "Error", "Del", "WoDel" );

            for ( Table_map_i i = tables.begin() ; i != tables.end() ; i ++ ) {

                Table *table = i->second;

                app.info("%-14s %-27s %7d %7d %7d %7d %7d %7d %7d", table->_type, table->_pg_table_name,
                        table->_stats.insert,
                            table->_stats.duplicate,
                        table->_stats.clean,
                        table->_stats.update,
                        table->_stats.error,
                        table->_stats.deleted,
                        table->_stats.would_delete
                        );


            }

        } else {

            app.info("%-14s %-27s %7s %7s %7s %7s %7s %7s",
                "Type", "Table Name", "Ins", "Dup", "Clean", "Update", "Error", "Del" );

            for ( Table_map_i i = tables.begin() ; i != tables.end() ; i ++ ) {

                Table *table = i->second;

                app.info("%-14s %-27s %7d %7d %7d %7d %7d %7d", table->_type, table->_pg_table_name,
                        table->_stats.insert,
                            table->_stats.duplicate,
                        table->_stats.clean,
                        table->_stats.update,
                        table->_stats.error,
                        table->_stats.deleted
                        );

            }

        }

    }

    void Table::clear( void ) {

        for ( Table_map_i i = tables.begin() ; i != tables.end() ; i ++ ) {

            Table *table = i->second;

            if ( table->lock() ) {
                /* some tables shouldn't be purged */
                continue;
            }

            Row_map_t &row = table->_rows;
            Row_map_i r;

            while (( r = row.begin() ) != row.end() ) {
                delete r->second;
                row.erase( r );
            }

            table->_rows_by_pkey.clear();

            table->_stats.clear();

        }

    }

    void Table::describe( void ) {
#if 0
        zdebug( _debug, "Describe table %s (type %s)\n", _pg_table_name, _type );

        for ( findex_t ctr = 0 ; ctr <  _fields_count ; ctr ++ ) {
            _fields[ ctr ]->describe();
        }
#endif

    }

    Field *Table::field( findex_t index ) {
        if (( index < 0 )||( index > _fields_count )) {
            throw( AppExcept("filed %d is out of range (max %d)", index, _fields_count ) );
        } else {
            return( _fields[ index ] );
        }
    }

    int Table::findField( const char *fieldname ) {
        for ( findex_t ctr = 0 ; ctr <  _fields_count ; ctr ++ ) {
            if ( strcmp( _fields[ ctr ]->name(), fieldname ) == 0 ) {
                return( ctr );
            }
        }
        return( -1 );
    }

    int Table::findField( const char *fieldname, size_t len ) {
        for ( findex_t ctr = 0 ; ctr <  _fields_count ; ctr ++ ) {
            if ( _fields[ ctr ]->nameLen() != len ) {
                continue;
            } else if ( strncmp( _fields[ ctr ]->name(), fieldname, len ) == 0 ) {
                return( ctr );
            }
        }
        return( -1 );
    }

    void Table::debug( bool value ) {
        _debug = value;
    }

    Row *Table::rowFindOrAllocate( std::string &index, bool *found ) {

        Row_map_i i;

        if (( i = _rows.find( index )) == _rows.end() ) {

            /* new row */

            zdebug( _debug, "%s: creating new row\n", _pg_table_name );

            Row *row = new Row( this, _fields_count );
            _rows.insert( Row_pair_t( index, row ) );

            *found = false;

            return( row );

        } else {

            /* existing row */

            zdebug( _debug, "%s: found existing row\n", _pg_table_name );

            *found = true;

            return( i->second );

        }

    }

    bool Table::injectValue( findex_t value_index ) {

        /* Step #1 - recurse adding values until all fields are examined */

        if ( value_index == _fields_count ) {
            return( injectAction( _values ) );
        }

        Field *field = _fields[ value_index ];

        zdebug( _debug, "%s: injectValue( %s with value_index = %d )\n",
                _pg_table_name,
                field->name(),
                value_index );

        Data        *data;
        Table       *ref_table;
        findex_t    ref_index;

        _values[ value_index ].value = NULL;
        _values[ value_index ].isset = false;

        if (( data = field->data() ) != NULL ) {

            zdebug( _debug, "field->data() != NULL\n" );

            if ( data->iterator() ) {

                zdebug( _debug, "field->iterator() == true\n" );

                for ( const char *value = data->begin() ; value ; value = data->next() ) {
                    _values[ value_index ].value = value;
                    _values[ value_index ].isset = true;

                    zdebug( _debug, "%s: _values[ %d ].value = %s\n", _pg_table_name, value_index, _values[ value_index ].value );
                    injectValue( value_index + 1 );
                }

                return( true );

            } else {

                zdebug( _debug, "field->iterator() == false\n" );

                _values[ value_index ].value = field->fetch();
                _values[ value_index ].isset = true;

                zdebug( _debug, "%s: _values[ %d ].value = %s\n", _pg_table_name, value_index, _values[ value_index ].value );

            }

        } else {

            zdebug( _debug, "field->data() == NULL\n" );

            if ((( ref_table = field->references( &ref_index )) != NULL )&&( ref_table->last() )) {

                if ( ref_table->last()->isset( ref_index ) ) {

                    _values[ value_index ].value = ref_table->last()->fetch( ref_index );
                    _values[ value_index ].isset = true;

                } else {

                    throw( AppExcept("DB Cache error: last row of %s field %s is not defined",
                            ref_table->name(),
                            field->name() ) );

                    /* the source is not defined, so make sure the copy is marked the same */

                    _values[ value_index ].value = NULL;
                    _values[ value_index ].isset = false;

                }

            }

        }

        zdebug( _debug, "field->prepareTrackable()\n");

        /* obtain access to the "job" TRES value */
        field->prepareTrackable( _row_count );

        injectValue( value_index + 1 );

        return( true );
    }

    bool Table::injectAction( Value_t *values ) {

        /*
         * Step #2 - recursion completed / values populated - next construct an index
         *
         * NOTE: When constructing the Index each field MUST have a value
         *       If a field doesn't have a value then an error is thrown.
         */

        std::string index;

        for ( findex_t ctr = 0 ; ctr < _indexes_count ; ctr ++ ) {

            if ( ctr > 0 ) {
                index += INDEX_SEPARATOR;
            }

            findex_t findex = _indexes[ ctr ];
            Field *field = _fields[ findex ];

            if ( ! values[ findex ].isset ) {

                throw( AppExcept("%s: field '%s' value has not been defined - mandatory for an index",
                            _pg_table_name, field->name() ) );

            } else if ( values[ findex ].value == NULL ) {
                /* this conditional is a redundant check - as the field value should not be set */

                throw( AppExcept("%s: field '%s' value IS NULL - mandatory for an index",
                            _pg_table_name, field->name() ) );

            }

            /* append the value to the std::string */
            index += values[ findex ].value;

        }

        zdebug( _debug, "%s: built index '%s'\n", _pg_table_name, index.c_str() );

        /* step #3 - check if this record already exists (or not) */

        Row *row;
        bool found;

        row = rowFindOrAllocate( index, &found );

        zdebug( _debug, "%s: row of index %s %s\n",
                _pg_table_name,
                index.c_str(),
                found ? "already exists" : "is not loaded"
                );

        /*
         * copy the values to the row
         *
         * NOTE: even the 'pkey' is copied here as it is excluded
         *       when doing updates or inserts.
         */

        for ( findex_t ctr = 0 ; ctr < _fields_count ; ctr ++ ) {
            if ( values[ ctr ].isset ) {
                row->update( ctr, values[ ctr ].value );
            }
        }

        for ( findex_t ctr = 0 ; ctr < _fields_count ; ctr ++ ) {

            findex_t i;

            if (( i = _fields[ ctr ]->isTrackable( ctr, row )) >= 0 ) {

                zdebug( _debug, "%s: manage trackable %s\n",
                        _pg_table_name, _fields[ ctr ]->name() );

#if 0
                for ( findex_t z = 0 ; z < _fields_count ; z ++ ) {
                    printf("FIELD %d %s=%s\n", z, _fields[ z ]->name(), row->fetch( z ) );
                }
#endif

            }

        }

        _last = row;

        zdebug( _debug, "%s: _last(%s) is set\n", _pg_table_name, index.c_str() );

        /* Step #5 - insert / update as necessary */

        if ( ! row->dirty() ) {

            /* row matches the database */
            _stats.clean ++;
            zdebug( _debug, "%s: injectValue finished [clean]\n", _pg_table_name );

            return( true );

        } else if ( ! found ) {
            
            /* record doesn't exist . . */
            try {
                this->insert( row );
            } catch ( AppExcept &e ) {
                throw( AppExcept("%s: insert failed for %s: %s", _pg_table_name, index.c_str(), e.what() ) );
            }

            zdebug( _debug, "[%s] injectValue finished [insert]\n", _pg_table_name );
            return( true );

        } else {

            try {
                this->update( row );
            } catch ( AppExcept &e ) {
                throw( AppExcept("%s: update failed for %s: %s", _pg_table_name, index.c_str(), e.what() ) );
            }

            zdebug( _debug, "[%s] injectValue finished [update]\n", _pg_table_name );
            return( true );

        }

        return( true );
    }

    const char *Table::inject( Table *table ) {

        _row_count ++;

        zdebug( _debug, "slurm( table: %s ) begin\n", _pg_table_name );

        injectValue( 0 );

        zdebug( _debug, "slurm( table: %s ) end\n", _pg_table_name );

        return( NULL );
    }

    void Table::purge( void ) {

        /* deleting records has a bug when multiple clusters are
         * injecting data */
#ifdef NO_DELETE
        _stats.deleted ++;
        return;
#endif

        zdebug( _debug, "slurm( table: %s ) purge (%ld rows present)\n", _pg_table_name, _rows.size() );

        int unused = 0;

        Row_map_t::iterator i = _rows.begin();

        while ( i != _rows.end() ) {

            Row_map_t::iterator j = i ++;

            if ( j->second->used() ) {
                /* the row is in use, so don't delete it from the database */
                continue;
            }

            Row *row = j->second;

            /* test if the row is in use */


            if ( ! row->parent() ) {

                /* row has no parent, so the use status cannot be trusted */
                continue;

            } else if ( ! row->parent()->used() ) {

                /* row has a parent, but it's not been referenced, so the
                 * data could be from another cluster - don't touch this record */
                continue;

            } else if ( app.nodelete() ) {

                /* no delete is defined - so don't handle any other logic */
                _stats.would_delete ++;
                continue;

            }

            /*
             * row has a parent, and the parent row is active, so if
             * this row isn't active it should be deleted
             */

            zdebug( _debug, "%s: delete row with key %s\n",
                    _pg_table_name, j->first.c_str() );

            _rows.erase( j );

            unused ++;

            int ctr;

            Query query;

            query.msql("DELETE FROM %s WHERE ", _pg_table_name );

            for ( ctr = 0 ; ctr < _indexes_count ; ctr ++ ) {

                if ( ctr > 0 ) {
                    query.sql += " AND ";
                }

                char *tmp = mprintf( "%s = $%d::%s",
                        _fields[ ctr ]->name(),
                        ctr + 1,
                        _fields[ ctr ]->cast() );

                query.sql += tmp;

                mfree( tmp );

                query.parameter( row->fetch( _indexes[ ctr ] ) );

            }

            if ( app.debug( DEBUG_DELETE ) ) {
                query.dump( __FILE__, __LINE__ );
            }

            if ( pg.execute( query ) < 0 ) {
                throw( AppExcept( "failed to select rows from %s: %s", _pg_table_name, pg.errorMessage() ) );
            } else if ( pg.resultStatus() == PGRES_COMMAND_OK ) {
                _stats.deleted ++;
            } else {
                query.dump( __FILE__, __LINE__ );
#warning FIXME: postgres error may span more than one line
                app.error("PG %s", pg.errorMessage() );
                throw( AppExcept( "query '%s' failed to return tuples (error code %d)",
                _pg_table_name, pg.resultStatus() ) );
            }

        }

        _rows_by_pkey.clear();

        zdebug( _debug, "%s - unused rows %d\n", _pg_table_name, unused );

    }

    void Table::insertByKey( findex_t index, const char *value ) {

        if ( index != _pkey ) {

            throw( AppExcept( "%s: insertByKey for field '%s' fail: must be primary key",
                        _pg_table_name, _fields[ index ]->name() ) );

#if 0
        } else if ( _fields_count != 1 ) {

            throw( AppExcept( "%s: insertByKey for field '%s' fail: table has > 1 fields (%d)",
                        _pg_table_name, _fields[ index ]->name(), _fields_count ) );

#endif
        } else if ( _indexes_count != 1 ) {

            throw( AppExcept( "%s: insertByKey for field '%s' fail: table has > 1 index fields (%d)",
                        _pg_table_name, _fields[ index ]->name(), _indexes_count ) );

        }

        Value_t *params = (Value_t*)calloc( sizeof( Value_t ), _fields_count );

        params[ index ].value = value;
        params[ index ].isset = true;

        /* inject the row */

        injectAction( params );

        mfree( params );

    }

    void Table::resolve( void ) {

        for ( Table_map_i i = tables.begin() ; i != tables.end() ; i ++ ) {

            Table *table = i->second;

            for ( findex_t count = 0 ; count < table->_fields_count ; count ++ ) {
                try {
                    table->_fields[ count ]->resolve();
                } catch ( AppExcept &e ) {
                    throw AppExcept("table '%s' field '%s': %s", table->name(), table->_fields[count]->name(), e.what() );
                } 
            }

        }

    }

    bool Table::makeJoinedQuery( Query &query ) {

        //printf("BUILDING JOIN FOR %s / %s\n", _pg_table_name, _type );

        /* step #1 - figure out which tables are referenced */

        Table        *ref_table = NULL;
        int             ref_field = -1;
        findex_t   field_index;

        for ( field_index = 0 ; field_index < _fields_count ; field_index ++ ) {

            //printf("testing %s / %d = %s\n", _pg_table_name, field_index, _fields[ field_index ]->name() );

            if (( ref_table = _fields[ field_index ]->references( &ref_field )) != NULL ) {

                if ( strcmp( ref_table->type(), "job" ) == 0 ) {
                    //printf("MATCH JOB\n");
                    break;
                } else if ( strcmp( ref_table->type(), "step" ) == 0 ) {
                    //printf("MATCH JOB\n");
                    break;
                }

                ref_table = NULL;
                ref_field = -1;

            }
        
        }

        if ( ref_table == NULL ) {
            return( false );
        }

        //printf("ref_table = %s ref_field = %d\n", ref_table->name(), ref_field );

        query.msql("SELECT %s.* FROM %s, %s WHERE",
                _pg_table_name,
                _pg_table_name,
                ref_table->name() );

        char *tmp = mprintf(" %s.%s = %s.%s AND",
                _pg_table_name,
                _fields[ field_index ]->name(),
                ref_table->name(),
                ref_table->field( ref_field )->name() );

        query.sql += tmp;

        mfree( tmp );

        return( true );
    }

    const char *Table::getValue( findex_t index ) {
        return( _values[ index ].value );
    }

    Row *Table::getByIndex( const char *index ) {
        Row_map_i i;
        if (( i = _rows.find( index )) != _rows.end() ) {
            return( i->second );
        } else {
            return( NULL );
        }
    }

    Row *Table::getByPrimaryIndex( const char *index ) {
        Row_map_i i;
        if (( i = _rows_by_pkey.find( index )) != _rows.end() ) {
            return( i->second );
        } else {
            return( NULL );
        }
    }

    void Table::setTrackable( Field *field ) {
        _field = field;
    }

    const char *Table::index( Row *row ) {

        _index_current = "";

        for ( int ctr = 0 ; ctr < _indexes_count ; ctr ++ ) {
            if ( ctr > 0 ) {
                _index_current += "|";
            }
            const char *tmp = row->fetch( _indexes[ ctr ] );
            _index_current += ( tmp ? tmp : "NULL" );
        }

        return( _index_current.c_str() );
    }

}

/* END OF FILE */

