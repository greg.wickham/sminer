#
#***********************************************************************
#*  sminer - Synchronise a database with Slurm sacct records
#***********************************************************************
#* Copyright (C) 2021 Greg Wickham <greg@wickham.me>
#*
#* This file is part of sminer.
#*
#*  sminer is free software: you can redistribute it and/or modify
#*  it under the terms of the GNU General Public License as published by
#*  the Free Software Foundation, either version 3 of the License, or
#*  (at your option) any later version.
#*
#*   sminer is distributed in the hope that it will be useful,
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#*   GNU General Public License for more details.
#*
#*   You should have received a copy of the GNU General Public License
#*   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
#**********************************************************************/
#

#
# rpm SPEC file for sminer
#

%define _name        sminer
%define _version     %{?VERSION}%{?!VERSION:1}
%define _release     %{?RELEASE}%{?!RELEASE:1}

%define buildroot %{_topdir}/%{_name}-%{_version}-root

BuildRoot:      %{buildroot}
Summary:        Slurm Miner - Translate from sacct to SQL
License:        GPLv3
Name:           %{_name}
Version:        %{_version}
Release:        %{_release}%{?dist}
Source:         %{name}-%{version}.tar.bz2
Group:          Development/Tools
BuildRequires:  yaml-cpp-devel postgresql-devel libsigc++20-devel jsoncpp-devel

%description

sminer extacts Job accounting information
from Slurm using sacct and then injects
it into a PostgreSQL database.

%prep

%setup -q -n %{name}-%{version}

%build

make

%install

mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/usr/share/%{name}
mkdir -p $RPM_BUILD_ROOT/etc
mkdir -p $RPM_BUILD_ROOT/var/lib/sminer

make install DESTDIR=$RPM_BUILD_ROOT

%files

%defattr(-,root,root)

/usr/share/%{name}
/usr/bin/sminer

%attr(0711,root,root) /var/lib/sminer

%config /etc/sminer.yaml

