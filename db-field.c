/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "db-field.h"
#include    "slurm.h"
#include    "data.h"
#include    "db-table.h"
#include    "app.h"
#include    "data-nodelist.h"

#include    "data-elapsed.h"

#undef DEBUG_JOB

namespace DB {

    Field::Field( Table *table, const char *name, const char *parser,
            const char *format, const char *slurm, const char *references,
            const char *default_value, const char *cast,
            const char *calc, const char *trackable ) {
        _table = table;
        _name = xstrdup( name );
        _name_len = xstrlen( _name );
        _parser = xstrdup( parser );
        _slurm = xstrdup( slurm );
        _format = xstrdup( format );

        memset( &_ref, 0, sizeof( _ref ) );
        _ref.text = xstrdup( references );

        memset( &_trackable, 0, sizeof( _trackable ) );
        _trackable[0].text = xstrdup( trackable );
        _formatter = NULL;


        if ( _format ) {

            if ( strcmp( _format, "elapsed" ) == 0 ) {
                _formatter = new DataElapsed( _name );
            } else {
                throw( AppExcept("field %s: format '%s' is not valid", _name, _format ) );
            }

        }

        _default = xstrdup( default_value );
        _cast = xstrdup( cast );
        _data = NULL;
        _data_index = 0;

        _tres_per_node = -1; /* force recalculate */
        _tres_to_consume = 0;
        _tres_remaining = 0;
        _tres_row = -1;
        _tres_rescan = false;

        if ( calc ) {
            _rpn.compile( calc );
        }

        zdebug( 0, "field[%s] slurm=%s parser=%s\n", _name, _slurm, _parser );

        if ( _slurm ) {
            try {
                _data = Data::connect( _slurm, &_data_index );
            } catch ( AppExcept &e ) {
                throw( AppExcept("field %s: %s", _name, e.what() ) );
            }
        }

    }

    Field *Field::make( Table *table, const char *name, const char *parser,
            const char *format, const char *slurm, const char *references,
            const char *default_value, const char *cast,
            const char *calc, const char *trackable ) {
        return( new Field( table, name, parser, format, slurm, references, default_value, cast, calc, trackable ) );
    }

    /*
     * FUNCTION:    name
     *
     * PARAM:       none
     *
     * RETURNS:     const char * - the database name of the field
     *
     * WHATITDOES:  returns the database name of the field
     */
    const char *Field::name( void ) {
        return( _name );
    }

    /*
     * FUNCTION:    nameLen
     *
     * PARAM:       none
     *
     * RETURNS:     size_t - the length of the database name
     *
     * WHATITDOES:  returns the length of the database name of this field
     */
    size_t Field::nameLen( void ) {
        return( _name_len );
    }

    /*
     * FUNCTION:    cast
     *
     * PARAM:       none
     *
     * RETURNS:     const char * - the database "cast" string for this field
     *
     * WHATITDOES:  returns casting information for the value of this field
     *              to be injected into the database
     */
    const char *Field::cast( void ) {
        return( _cast );
    }

    /*
     * FUNCTION:    fetch
     *
     * PARAM:       none
     *
     * RETURNS:     const char * - value of this field as stored in a Data object
     *
     * WHATITDOES:  queries the connected data object and returns the value;
     *              If the Data object doesn't have a stored value then NULL
     *              is returned.
     */

    const char *Field::fetch( void ) {

        if (( _data )&&( _data->isset() )) {

            const char *value = _data->value( _data_index );

            if ( ! value ) {

                /* no value - return NULL */
                return( NULL );

            }

            if ( _formatter ) {

                /* convert the value to another format */

                _formatter->parse( value, strlen( value ));

                value = _formatter->value( 0 );

            }

            if ( _rpn.defined() ) {

                /* RPN processor defined */

                int64_t number = 0;

                for ( ; *value ; value ++ ) {
                    number = ( number * 10 ) + ( *value - '0' );
                }

                number = _rpn.run( number );

                _number = std::to_string( (int64_t)number );

                return( _number.c_str() );

            } else {

                return( value );

            }

        } else {

            return( NULL );

        }

    }

    /*
     * FUNCTION:    type
     *
     * PARAM:       none
     *
     * RETURNS:     DataType - ID of the connected Data object
     *
     * WHATITDOES:  Passes the query to the connected Data object
     *              and returns the ID.
     */
    DataType Field::type( void ) {
        if ( _data ) {
            return( _data->type() );
        } else {
            return( DATATYPE_NONE );
        }
    }

    /*
     * FUNCTION:    resolve
     *
     * PARAM:       none
     *
     * RETURNS:     none
     *
     * THROWS:      Error if resolution fails
     *
     * WHATITDOES:  Based on the "_ref_text" (format of "tablename(fieldname)")
     *              the table and field is confirmed to exist otherwise an error
     *              is thrown. Once the field has been confirmed to exist then
     *              a reference (_ref_table) to the Table and the index number
     *              of the referenced field (_ref_index) is stored. A pointer
     *              (_ref_field) to the target field is also stored.
     */
    void Field::resolve( void ) {

        /* format of the text is "tablename(fieldname)" */

        std::string tname, tfield;

        if ( _trackable[0].text ) {

            std::string tname2, tfield2;

            try {

                std::string index;

                const char *text;

                text = extractDelimitedText( _trackable[0].text, index, ":" );
                text = extractDelimitedText( text, tname, "(" );
                text = extractDelimitedText( text, tfield, "):" );
                text = extractDelimitedText( text, tname2, "(" );
                text = extractDelimitedText( text, tfield2, ")" );

                if ( *text ) {
                    throw( AppExcept("bad format") );
                }

                /* set the index information */

                _trackable[0].table = _table;

                if (( _trackable[0].index = _trackable[0].table->findField( index.c_str() )) < 0 ) {
                    throw( AppExcept("table '%s' field '%s' not defined", tname.c_str(), tfield.c_str() ) );
                }

                _trackable[0].field = _trackable[0].table->field( _trackable[0].index );

                /* tables to lookup */

                if (( _trackable[1].table = Table::fetchByName( tname.c_str() )) == NULL ) {
                    throw( AppExcept("table '%s' table not defined", tname.c_str() ));
                }

                if (( _trackable[1].index = _trackable[1].table->findField( tfield.c_str() )) < 0 ) {
                    throw( AppExcept("table '%s' field '%s' not defined", tname.c_str(), tfield.c_str() ) );
                }

                _trackable[1].field = _trackable[1].table->field( _trackable[1].index );

                if (( _trackable[2].table = Table::fetchByName( tname2.c_str() )) == NULL ) {
                    throw( AppExcept("table '%s' table not defined", tname2.c_str() ));
                }

                if (( _trackable[2].index = _trackable[2].table->findField( tfield2.c_str() )) < 0 ) {
                    throw( AppExcept("table '%s' field '%s' not defined", tname2.c_str(), tfield2.c_str() ) );
                }

                _trackable[2].field = _trackable[2].table->field( _trackable[2].index );

                _table->setTrackable( this );

            } catch ( AppExcept &e ) {
                throw AppExcept("processing '%s': %s", _trackable[0].text, e.what() );
            }


        }

        if ( _ref.text ) {

            /* if a "reference: table(field)" is defined . . */

            try {

                const char *text;

                text = extractDelimitedText( _ref.text, tname, "(" );
                text = extractDelimitedText( text, tfield, ")" );

                if ( *text ) {
                    throw( AppExcept("bad format") );
                }

                if (( _ref.table = Table::fetchByName( tname.c_str() )) == NULL ) {
                    throw( AppExcept("table '%s' table not defined", tname.c_str() ));
                }

                if (( _ref.index = _ref.table->findField( tfield.c_str() )) < 0 ) {
                    throw( AppExcept("table '%s' field '%s' not defined", tname.c_str(), tfield.c_str() ) );
                }

            } catch ( AppExcept &e ) {
                throw AppExcept("processing '%s': %s", _ref.text, e.what() );
            }


            _ref.field = _ref.table->field( _ref.index );

        }

    }

    /*
     * FUNCTION:    references
     *
     * PARAMS:      findex_t*   - pointer to storage location for the ref field index
     *
     * RETURNS:     Table *     - pointer to a Table object
     *
     * THROWS:      none
     *
     * WHATITDOES:  If this field references another field then the field index is
     *              stored into *index and a pointer to the Table object is returned.
     *              Otherwise NULL is returned.
     */
    Table *Field::references( findex_t *index ) {
        if ( _ref.table ) {
            *index = _ref.index;
            return( _ref.table );
        } else {
            return( NULL );
        }
    }

    /* FUNCTION:    data
     *
     * PARAMS:      none
     *
     * RETURNS:     Data*   - pointer to the underlying Data object
     *
     * WHATITDOES:  Returns a pointer to the underlying Data object or NULL
     */
    Data *Field::data( void ) {
        return( _data );
    }

    /* FUNCTION:    isTrackable
     *
     * PARAMS:      none
     *
     * RETURNS:     findex_t - index of field in this table that is
     *              used as a key
     *
     */

    void Field::prepareTrackable( int row_count ) {

        if ( _trackable[0].text ) {

            if ( row_count == _tres_row ) {
                /* nothing to do */
                return;
            }

            _tres_row = row_count;

            _tres_rescan = true;

            const char *value = _trackable[1].table->getValue( _trackable[ 1 ].index );

            if ( value ) {

                _trackable[0].count = atoi( value );

#ifdef DEBUG_JOB
                printf("%s(%s) prepareTrackable - job allocation %d\n",
                        _trackable[1].table->name(),
                        _trackable[1].field->name(),
                        _trackable[0].count );
#endif

                _tres_to_consume = _trackable[0].count;

            } else {

                _tres_to_consume = 0;

            }

#ifdef DEBUG_JOB
            const char *tName = _trackable[1].table->name();
            const char *fName = _trackable[1].field->name();
#endif

#if 0
            /* fetch the number of nodes */

            int node_count = dynamic_cast<DataNodeList*>(
                    _trackable[ 0 ].table->field( _trackable[ 0 ].index )->data()
                    )->nodes().size();

            _tres_to_consume = atoi( _trackable[1].table->getValue( _trackable[1].index ) );
            _tres_per_node = _tres_to_consume / node_count;
            _tres_remaining = _tres_to_consume - ( _tres_per_node * node_count );

#ifdef DEBUG_JOB
            printf("%s(%s) prepareTrackable\n", tName, fName );
            printf("    _tres_per_node = %d\n", _tres_per_node );
            printf("    _tres_to_consume = %d\n", _tres_to_consume );
            printf("    _tres_remaining = %d\n", _tres_remaining );
            printf("    _tres_row = %d\n", _tres_row );
            printf("    node count = %d\n", node_count );
#endif

#if 0
            const char *value = _trackable[1].table->getValue( _trackable[ 1 ].index );

            if ( value ) {

                _trackable[0].count = atoi( value );

#ifdef DEBUG_JOB
                printf("%s(%s) prepareTrackable - job allocation %d\n",
                        _trackable[1].table->name(),
                        _trackable[1].field->name(),
                        _trackable[0].count );
#endif

                _tres_to_consume = _trackable[0].count;

            } else {

#ifdef XDEBUG_JOB
                printf("%s(%s) prepareTrackable - job allocation NO VALUE\n",
                        _trackable[1].table->name(),
                        _trackable[1].field->name() );

                _tres_to_consume = 0;
                _trackable[0].count = -1;
#endif

            }
#endif
#endif

        }

    }

    findex_t Field::isTrackable( findex_t row_index, Row *row ) {

        if ( ! _trackable[0].text ) {
            return( -1 );
        } else if ( _trackable[0].count < 0 ) {
            return( -1 );
        }

        /* field is trackable . . . */

        if ( _tres_rescan ) {
            /* calculate . . */

            _tres_rescan = false;

            int node_count = dynamic_cast<DataNodeList*>(
                    _trackable[ 0 ].table->field( _trackable[ 0 ].index )->data()
                    )->nodes().size();

            _tres_per_node = _tres_to_consume / node_count;
            _tres_remaining = _tres_to_consume - ( _tres_per_node * node_count );

        }

#ifdef DEBUG_JOB
        const char *tName = _trackable[1].table->name();
        const char *fName = _trackable[1].field->name();

        printf("%s(%s) isTrackable count=%d _tres_to_consume=%d\n",
                tName, fName, _trackable[0].count, _tres_to_consume );
#endif

        /* obtain the node name */

        //const char *node_name = _trackable[0].table->getValue( _trackable[0].index );
        //printf("XX isTrackable( %s ) = yes - node = %s\n", _name, node_name );

        int tres_this_node = 0;

        if ( _tres_to_consume > 0 ) {

            tres_this_node = _tres_per_node + _tres_remaining;
            _tres_to_consume -= tres_this_node;
            _tres_remaining = 0;

        }

        char number[ 32 ];

#ifdef DEBUG_JOB
        printf("%s(%s) setValue( %d )\n", tName, fName, tres_this_node );
#endif

        snprintf( number, sizeof( number ), "%d", tres_this_node );
        row->update( row_index, number );

        return( 0 );
    }

}

/* END OF FILE */
