/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "postgres.h"
#include    <yaml-cpp/yaml.h>
#include    "query.h"
#include    "app.h"

Postgres pg;

Postgres::Postgres() {
    _conn = NULL;
    _results = NULL;
}

void Postgres::configure( const YAML::Node &config ) {

    if ( config.Type() != YAML::NodeType::Map ) {
        throw( AppFatal("Postgres configuration defintion must be a YAML Map") );
    }

    /* save the configuration to be used later */

    _config = config;

}

void Postgres::connect( void ) {

    if ( _conn ) {
        /* already connected */
        return;
    }

    if ( _config["connect"].IsScalar() ) {

        _conn = PQconnectdb( _config["connect"].as<std::string>().c_str() );

    } else {

        YAML::Node  server = _config["server"];
        YAML::Node  database = _config["database"];
        YAML::Node  port = _config["port"];
        YAML::Node  options = _config["options"];
        YAML::Node  tty = _config["tty"];
        YAML::Node  username = _config["username"];
        YAML::Node  password = _config["password"];

        _conn = PQsetdbLogin(
                server.IsScalar() ? server.as<std::string>().c_str() : NULL,
                port.IsScalar() ? port.as<std::string>().c_str() : NULL,
                options.IsScalar() ? options.as<std::string>().c_str() : NULL,
                tty.IsScalar() ? tty.as<std::string>().c_str() : NULL,
                database.IsScalar() ? database.as<std::string>().c_str() : NULL,
                username.IsScalar() ? username.as<std::string>().c_str() : NULL,
                password.IsScalar() ? password.as<std::string>().c_str() : NULL
            );

    }

    if ( PQstatus( _conn ) != CONNECTION_OK ) {
        throw( AppFatal("postgres failed to connect: %s", 
                    PQerrorMessage( _conn ) ));
    }

}

int Postgres::execute( Query &query ) {

    if ( _results ) {
        PQclear( _results );
        _results = NULL;
    }

    if ( app.verbose ) {
        query.print();
    }

    return(( _results = query.execute( _conn )) == NULL ? -1 : 0 );
}

int Postgres::ntuples( void ) {
    if ( _results == NULL ) {
        return( -1 );
    }
    return( PQntuples( _results ) );
}

char *Postgres::fname( int column_number) {
    if ( _results == NULL ) {
        return( NULL );
    } else {
        return( PQfname( _results, column_number ) );
    }
}

int Postgres::nfields( void ) {
    if ( _results == NULL ) {
        return( -1 );
    } else {
        return( PQnfields( _results ) );
    }
}

char *Postgres::getvalue( int row_number, int column_number ) {
    if ( _results == NULL ) {
        return( NULL );
    } else {
        return( PQgetvalue( _results, row_number, column_number ) );
    }
}

int Postgres::getisnull( int row_number, int column_number ) {
    if ( _results == NULL ) {
        return( -1 );
    } else {
        return( PQgetisnull( _results, row_number, column_number ) );
    }
}

int Postgres::getlength( int row_number, int column_number ) {
    if ( _results == NULL ) {
        return( -1 );
    } else {
        return( PQgetlength( _results, row_number, column_number ) );
    }
}

int Postgres::resultStatus( void ) {

    if ( _results == NULL ) {

        return( -1 );

    } else {

        return( PQresultStatus( _results ) );

    }

}

const char *Postgres::errorMessage( void ) {
    return( PQerrorMessage( _conn ));
}

bool Postgres::errorContains( const char *text ) {
    return( contains( errorMessage() , text ) );
}

char *Postgres::resultErrorField( int fieldcode ) {
    return( PQresultErrorField( _results, fieldcode ) );
}

/* END OF FILE */
