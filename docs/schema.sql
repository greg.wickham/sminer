
--
-- Slurm Miner Sample PostgreSQL Schema
--

CREATE SEQUENCE slurm_job_index START WITH 1 INCREMENT BY 1;

--
-- If importing from multiple clusters it's convenient to separate
-- by cluster name.
--
CREATE TABLE slurm_cluster (
    name VARCHAR PRIMARY KEY NOT NULL
);

--
-- User table
--
CREATE TABLE slurm_user (
    username VARCHAR PRIMARY KEY NOT NULL
);

--
-- Node table
--
CREATE TABLE slurm_node (
    node_name VARCHAR PRIMARY KEY NOT NULL,
    gpu_model VARCHAR,
    gpu_count INTEGER
);

--
-- Job Table
--
CREATE TABLE slurm_job (
    job_index INTEGER PRIMARY KEY NOT NULL DEFAULT nextval('slurm_job_index'),
    job_id VARCHAR NOT NULL,
    username VARCHAR NOT NULL REFERENCES slurm_user( username ),
    account VARCHAR NOT NULL,
    cluster VARCHAR NOT NULL REFERENCES slurm_cluster( name ),
    partition VARCHAR NOT NULL,
    state VARCHAR NOT NULL,
    qos VARCHAR,
    reservation VARCHAR,
    priority INTEGER,
    time_submit TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    time_start TIMESTAMP WITHOUT TIME ZONE,
    time_end TIMESTAMP WITHOUT TIME ZONE,
    tres_cpu INTEGER NOT NULL,
    tres_gpu INTEGER NOT NULL,
    tres_memory INTEGER NOT NULL,
    timelimit INTEGER NOT NULL,
    nodes INTEGER,
    exitcode VARCHAR,
    -- as slurm can re-use a JobID, force a primary key with jobid and submit time
    UNIQUE( job_id, time_submit )
);

CREATE TABLE slurm_step (
    step_index INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('slurm_job_index'),
    job_index INTEGER NOT NULL REFERENCES slurm_job( job_index ),
    job_id VARCHAR NOT NULL,
    time_submit TIMESTAMP WITHOUT TIME ZONE,
    time_start TIMESTAMP WITHOUT TIME ZONE,
    time_end TIMESTAMP WITHOUT TIME ZONE,
    cputime BIGINT,
    state VARCHAR,
    alloc_cpu BIGINT,
    alloc_mem BIGINT,
    alloc_node BIGINT,
    alloc_gpu BIGINT,
    cpu_total BIGINT,
    cpu_user BIGINT,
    cpu_system BIGINT,
    avevmsize BIGINT,
    averss BIGINT,
    elapsed BIGINT,
    exitcode VARCHAR,
    maxdiskread BIGINT,
    maxdiskreadnode VARCHAR REFERENCES slurm_node( node_name ),
    maxdiskwrite BIGINT,
    maxdiskwritenode VARCHAR REFERENCES slurm_node( node_name ),
    maxrss BIGINT,
    maxrssnode VARCHAR REFERENCES slurm_node( node_name ),
    maxvmsize BIGINT,
    maxvmsizenode VARCHAR REFERENCES slurm_node( node_name ),
    mincpu BIGINT,
    mincpunode VARCHAR REFERENCES slurm_node( node_name ),
    UNIQUE( job_index, job_id, time_start )
);

CREATE TABLE slurm_job_node_link (
    job_index INTEGER NOT NULL REFERENCES slurm_job( job_index ),
    node_name VARCHAR NOT NULL REFERENCES slurm_node( node_name ),
    tres_gpu INTEGER,
    UNIQUE( job_index, node_name )
);

CREATE TABLE slurm_step_node_link (
    step_index INTEGER NOT NULL REFERENCES slurm_step( step_index ),
    node_name VARCHAR NOT NULL REFERENCES slurm_node( node_name ),
    tres_gpu INTEGER,
    UNIQUE( step_index, node_name )
);

-- !!OPTIONAL!!

--
-- VIEW: slurm_nodes
--
-- WHY? Joining with the slurm_job_node_link table results
--   in each node being on a separate line of the query results.
--   Using this view it is possible to join a job record with
--   the aggregated list of assigned nodes appearing as a single
--   column.
--

CREATE OR REPLACE VIEW slurm_job_nodes AS
    SELECT
        slurm_job_node_link.job_index,
        string_agg(
            slurm_job_node_link.node_name::text,
            ','::text
          ORDER BY
            (slurm_job_node_link.node_name::text)
          ) AS nodes,
        count(*) AS node_count
    FROM slurm_job_node_link
        GROUP BY slurm_job_node_link.job_index;

CREATE OR REPLACE VIEW slurm_step_nodes AS
    SELECT
        slurm_step_node_link.step_index,
        string_agg(
            slurm_step_node_link.node_name::text,
            ','::text
          ORDER BY
            (slurm_step_node_link.node_name::text)
          ) AS nodes,
        count(*) AS node_count
    FROM slurm_step_node_link
        GROUP BY slurm_step_node_link.step_index;

-- END OF FILE
