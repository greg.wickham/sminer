/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "mylib.h"

int debug = 0;

char *mprintf( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    char *text = vmprintf( fmt, ap );
    va_end( ap );
    return( text );
}

char *vmprintf( const char *fmt, va_list ap ) {
    va_list aq;
    va_copy( aq, ap );
    int len = vsnprintf( NULL, 0, fmt, ap ) + 1;
    char *text = (char*)malloc( sizeof( char ) * len );
    vsnprintf( text, len, fmt, aq );
    return( text );
}

size_t xstrlen( const char *s ) {
    return( s ? strlen( s ) : 0 );
}

char *xstrdup( const char *text ) {
    return( text ? strdup( text ) : NULL );
}

char *xstrndup( const char *text, size_t len ) {
    return( text ? strndup( text, len ) : NULL );
}

int strcasecmp( std::string &s1, const char *s2 ) {
    return( strcasecmp( s1.c_str(), s2 ) );
}

size_t toMBytes( const char *text, size_t len ) {

    size_t value = 0;
    size_t ctr;

    for ( ctr = 0 ; ctr < len ; ctr ++ ) {

        if ( ! isdigit( text[ ctr ] ) ) {
            break;
        }

        value = value * 10 + text[ ctr ] - '0';

    }

    if ( ctr != len ) {

        switch( text[ ctr ] ) {
            case 'M':
                break;
            default:
                printf("WHAT TO DO WITH %c\n", text[ ctr ] );
                printf("%ld\n", value );
                exit( 1 );
                break;
        }
    }

    return( value );
}

bool contains( const char *string, const char *substring ) {

    size_t len = strlen( substring );

    for ( ; *string ; string ++ ) {

        if ( strncasecmp( string, substring, len ) == 0 ) {
            return( true );
        }

    }

    return( false );
}

time_t toNumber( const std::string &text ) {
    return( toNumber( text.c_str(), text.size() ) );
}

time_t toNumber( const char *text, size_t len ) {

    time_t tmp = 0;

    for ( size_t ctr = 0 ; ctr < len ; ctr ++ ) {
        if ( ! isdigit( text[ ctr ] ) ) {
#if 0
            printf("invalid character when expecting a digit - %c\n", text[ ctr ] );
#endif
            throw( 40 );
        }
        tmp = ( tmp * 10 ) + text[ ctr ] - '0';
    }

    return( tmp );
}

int xstrcmp( const char *s1, const char *s2 ) {

    if ( ! s1 || ! s2 ) {
        return( -1 );
    } else {
        return( strcmp( s1, s2 ) );
    }

}

int xstrcasecmp( const char *s1, const char *s2 ) {

    if ( ! s1 || ! s2 ) {
        return( -1 );
    } else {
        return( strcmp( s1, s2 ) );
    }

}

static void numberPush( string_list_t *lp1, string_list_t *lp2, int number, int len ) {

    char *fmt = mprintf( "%%0%dd", len );
    char *text = mprintf( fmt, number );

    for ( string_list_i i = lp1->begin() ; i != lp1->end() ; i ++ ) {
        lp2->push_back( *i + text );
    }

    mfree( text );
    mfree( fmt );

}

static void numberSwap( string_list_t **rp1, string_list_t **rp2 ) {
    string_list_t *tmp = *rp1;
    *rp1 = *rp2;
    *rp2 = tmp;
    (*rp2)->clear();
}

#if 0
static void dump( string_list_t *rp1 ) {

    printf("DUMP\n");
    for ( string_list_i i = rp1->begin() ; i != rp1->end() ; i ++ ) {
        printf("-- %s\n", (*i).c_str() );
    }

}
#endif

void nodeExpand( string_list_t &nodes, const std::string &text ) {

    const char *ptr = text.c_str();

    bool bracket_is_open = false;

#if 0
    printf("PROCESSING %s\n", text.c_str() );
#endif

    int ctr = 0;

    string_list_t   list1, *lp1 = &list1;
    string_list_t   list2, *lp2 = &list2;

    list1.push_back("");

    bool            range = false;
    int             number = 0;
    int             number_len = 0;
    int             number_lower = -1;
    int             number_lower_len = 0;

    while ( true ) {

#if 0
        printf("process '%c' bracket_is_open = %d\n", ptr[ ctr ], bracket_is_open );
#endif

        if ( bracket_is_open ) {
            /* brackets are active - options are now limited */

            switch ( ptr[ ctr ] ) {

                case '\0':   /* \0 character should never happen within brackets */
                    throw( 23 );
                    break;

                case ']':    /* close bracket found */

#if 0
                    printf("bkt - close bracket found\n");
#endif

                    if ( range ) {

                        if ( number_len != number_lower_len ) {
                            throw( 6 );
                        }

                        for ( int i = number_lower ; i <= number ; i ++ ) {
                            numberPush( lp1, lp2, i, number_len );
                        }

                        number_lower = -1;

                        number = 0;
                        number_len = 0;

                        range = false;

                    } else {

#if 0
                        printf("',' found with %d (%d)\n", number, number_len );
#endif

                        numberPush( lp1, lp2, number, number_len );

                        number = 0;
                        number_len = 0;

                    }

                    /* discard lp1; and swap lp2 to be lp1 */
                    numberSwap( &lp1, &lp2 );

                    /* closing the bracket */
                    bracket_is_open = false;

                    break;

                case '-':    /* hypen - specifies a range */

#if 0
                    printf("bkt - hypen found\n");
#endif

                    if ( range ) {
                        /* can't have two hypens */
                        throw( 5 );
                    }

                    number_lower = number;
                    number_lower_len = number_len;

                    number = 0;
                    number_len = 0;

                    break;

                case ',':    /* comma - specifies multiple values */

#if 0
                    printf("bkt - comma found\n");
#endif

                    if ( range ) {

                        if ( number_len != number_lower_len ) {
                            throw( 6 );
                        }

                        for ( int i = number_lower ; i <= number ; i ++ ) {
                            numberPush( lp1, lp2, i, number_len );
                        }

                        number_lower = -1;

                        number = 0;
                        number_len = 0;

                        range = false;

                    } else {
                        
#if 0
                        printf("',' found with %d (%d)\n", number, number_len );
#endif

                        numberPush( lp1, lp2, number, number_len );

                        number = 0;
                        number_len = 0;

                    }

                    break;

                case '0' ... '9':

                    /* convert characters to a number */
                    number = ( number * 10 ) + ptr[ ctr ] - '0';
                    number_len ++;

                    break;
                default:

                    /* any other character is not permitted */
                    printf("INVALID %c\n", ptr[ ctr ] );
                    throw( 4 );
                    break;
            }
            
            ctr ++;
            continue;
        }

        if (( ptr[ ctr ] == '\0' )||( ptr[ ctr ] == ',' )) {
            /* an end of string character or a comma outside of a bracket set */

            for ( string_list_i i = lp1->begin() ; i != lp1->end() ; i ++ ) {
                nodes.push_back( *i );
            }

            if ( ptr[ ctr ] == '\0' ) {
                break;
            }

            lp1->clear();
            lp2->clear();
            lp1->push_back("");

            ptr = &ptr[ ctr + 1 ];
            ctr = 0;

            continue;

        } else if ( ptr[ctr] == '[' ) {
            /* bracket set is not open -  */

            bracket_is_open = true;
            number = 0;
            number_len = 0;

        } else if ( ptr[ ctr ] == ']' ) {

            /* error - close bracket without an active open */
            throw( 3 );

        } else {
            /* add this character to all names */
            for ( string_list_i i = lp1->begin() ; i != lp1->end() ; i ++ ) {
                *i += ptr[ ctr ];
            }
        }

        ctr ++;

    }

}

const char *xstrnchr( const char *str, size_t len, char c ) {

    for ( size_t ctr = 0 ; ctr < len ; ctr ++ ) {
        if ( str[ ctr ] == c ) {
            return( &str[ ctr ] );
        }
    }

    return( NULL );
}

void _zdebug( const char *filename, int line, const char *func, const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    char *tmp = vmprintf( fmt, ap );
    va_end( ap );

    printf("[DEBUG %s %d %s] %s", filename, line, func, tmp );
    mfree( tmp );
}

bool yamlNodeBoolean( const YAML::Node &node, const char *key, bool def ) {

    YAML::Node element = node[ key ];

    try {
        if ( element.Type() != YAML::NodeType::Scalar ) {
            return( def );
        }
    } catch ( const YAML::InvalidNode &e ) {
        return( def );
    }

    const char *text = element.as<std::string>().c_str();

    return( booleanFromString( text ) );
}

bool booleanFromString( const char *text ) {

    static const char *bools[] = {
        "yes",
        "on",
        "true",
        "1",
        NULL
    };
    
    for ( int ctr = 0 ; bools[ ctr ] ; ctr ++ ) {
        if ( strcasecmp( bools[ ctr ], text ) == 0 ) {
            return( true );
        }
    }
    return( false );
}

time_t rewindToMidnight( time_t value ) {

    struct tm *tm = localtime( &value );

    tm->tm_hour = 0;
    tm->tm_sec = 0;
    tm->tm_min = 0;

    return( mktime( tm ) );
}

time_t stringToTime( const char *text ) {
    return( 0 );
}

/*
 * FUNCTION: extractTableAndField
 *
 *          extracts tname and tfield from text with format 'table(field)'
 *
 * RETURNS:
 *
 *      Pointer to the last character matched.
 *      Calling functiion must check for *return value == '\0' for success
 */

const char *extractDelimitedText( const char *text, std::string &dst, const char *endstr ) {

    if ( *text == '\0' ) {
        throw( AppExcept( "text string is empty" ) );
    }

    size_t endstrlen = strlen( endstr );

    while ( true ) {

        if ( ! *text ) {
            /* return a pointer to \0 */
            return( text );
        } else if ( strncmp( text, endstr, endstrlen ) == 0 ) {
            /* return a pointer to the first character after the delimiter */
            return( text + endstrlen );
        } else {
            /* add this character to the destination */
            dst += *text++;
        }
    }
}

/* END OF FILE */
