/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "app.h"
#include    "args.h"

#include    <json/writer.h>

extern const char *__progname;

LOG_DEFN App::log_levels[] = {
    { LOG_EMERG, "EMERG", "system is unusable" },
    { LOG_ALERT, "ALERT", "action must be taken immediately" },
    { LOG_CRIT, "CRIT", "critical conditions" },
    { LOG_ERR, "ERROR", "error conditions" },
    { LOG_WARNING, "WARNING", "warning conditions" },
    { LOG_NOTICE, "NOTICE", "normal, but significant, condition" },
    { LOG_INFO, "INFO", "informational message" },
    { LOG_DEBUG, "DEBUG", "debug-level message" },
    { 0, NULL, NULL }
};

App app;

int logLevelToValue( const char *text ) {

    size_t len = strlen( text );

    for ( int i = 0 ; App::log_levels[ i ].text ; i ++ ) {

        if ( strncasecmp( text, App::log_levels[ i ].text, len ) == 0 ) {
            return( App::log_levels[ i ].priority );
        }

    }

    throw( AppFatal("log level '%s' is not valid", text ) );

}

App::App() {
    verbose = false;
    en_stdout = false;
    updates = false;
    saveRunning = false;
    _time = 0;
    _syslog = false;
    _fp_log = NULL;
    _fp_log_filename = NULL;
    _use_stdout = true;
    _priority = logLevelToValue( "info" );
    _errors = 0;
    _alerts = 0;
    _infos = 0;
    _debugs = 0;
    _rc = 0;
    _start = time( NULL );
    _nodelete = false;
    program = basename( __progname );
}

void App::error( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    log( LOG_ERR, fmt, ap );
    va_end( ap );
    _errors += 1;
}

void App::info( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    log( LOG_INFO, fmt, ap );
    va_end( ap );
    _infos += 1;
}

void App::alert( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    log( LOG_ALERT, fmt, ap );
    va_end( ap );
    _alerts += 1;
}

void App::debug( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    log( LOG_DEBUG, fmt, ap );
    va_end( ap );
    _debugs += 1;
}

void App::log( int priority, const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    log( priority, fmt, ap );
    va_end( ap );
}

void App::log( int priority, const char *fmt, va_list ap ) {

    if ( priority > log_levels[ _priority ].priority ) {
        /* discard anything greater than the set priority level */
        return;
    }

    time_t now = time( NULL );

    if ( _time != now ) {
        _time = now;
        const char *format = "%Y-%m-%d %H:%M:%S";
        strftime( _time_text, sizeof( _time_text ) - 1, format, localtime( &now ));
        _time_text[ sizeof( _time_text ) - 1 ] = '\0';
    }

    if ( _syslog ) {
        /* log the raw message to syslog if that is configured */
        vsyslog( priority, fmt, ap );
    }

    FILE *fp[] = {
        _fp_log,
        _use_stdout || en_stdout ? stdout : NULL
    };

    int index;

    for ( index = 0 ; log_levels[ index ].priority >= 0 ; index ++ ) {
        if ( log_levels[ index ].priority == priority ) {
            break;
        }
    }

    char *line = NULL;

    for ( int ctr = 0 ; ctr < 2 ; ctr ++ ) {

        if ( fp[ ctr ] == NULL ) {
            continue;
        } else if ( line == NULL ) {
            char *tmp = mprintf("[%s] %s: %s", _time_text, log_levels[ index ].text, fmt );
            line = vmprintf( tmp, ap );
            mfree( tmp );
        }

        fprintf( fp[ ctr ], "%s\n", line );

        fflush( fp[ ctr ] );
    }

    /* log to syslog if configured */
    if ( _syslog ) {
        vsyslog( log_levels[ index ].priority, fmt, ap );
    }

    /* save this message for later writing to the state file */

    if ( line ) {
        _messages.push_back( line );
        mfree( line );
    }

}

void App::configure( YAML::Node map ) {

    if ( map.Type() == YAML::NodeType::Null ) {
        /* nothing to do */
        return;
    } else if ( map.Type() != YAML::NodeType::Map ) {
        throw( AppFatal( "Logging configuration requires a YAML Map") );
        exit( 1 );
    }

    /* redirect output to stdout too? */

    _use_stdout = yamlNodeBoolean( map, "stdout", true );

    if ( map["file"].Type() == YAML::NodeType::Scalar ) {

        if ( _fp_log_filename ) {
            mfree( _fp_log_filename );
            fclose( _fp_log );
            _fp_log = NULL;
        }

        _fp_log_filename = strdup( map["file"].as<std::string>().c_str() );

        if (( _fp_log = fopen( _fp_log_filename, "a+" )) == NULL ) {
            throw( AppFatal( "%s: %s\n", _fp_log_filename, strerror( errno )));
        }

        app.info("writing log to %s", _fp_log_filename );

    }

    /* and finally open syslog if required */

    if ( yamlNodeBoolean( map, "syslog", false ) ) {
        openlog( program, LOG_NDELAY | LOG_PID, LOG_DAEMON );
        _syslog = true;
    }

    /* set the level priority */
    YAML::Node pnode = map["priority"];

    if ( pnode.IsScalar() ) {
        _priority = logLevelToValue( pnode.as<std::string>().c_str() );
    }

}

/*
 * FUNCTION:    configure
 *
 * PARAMETERS:  Args & - reference the command line arguments
 *
 * WHATITDOES:  After the configuration is loaded from the configuration
 *              file the command line can be used to override.
 */
void App::configure( Args &parser ) {

    const char *text;

    if (( text = parser["priority"] ) != NULL ) {
        _priority = logLevelToValue( text );
    }

    if ( parser.defined( "stdout" ) ) {
        _use_stdout = true;
    }

    alert("%s %s started", PROG_NAME, PROG_VERSION );

}

/*
 * FUNCTION:    summary
 *
 * PARAMETERS:  None
 *
 * WHATITDOES:  If any error messages were displayed, display the 
 *              error message count.
 */
void App::summary( void ) {

    if ( _errors > 0 ) {
        app.error("%d error messages were displayed", _errors );
        app.error("Any error messages being displayed are indicative of");
        app.error("an issue that needs to be resolved.");
        app.error("Database records maybe incomplete");
    }

}

void App::exit( int rc ) {

    _rc = rc;

    ::exit( rc );
}

void App::setDebug( const char *param ) {

    /* set the debug flag */
    _debug_flag = atoi( param );

}

bool App::debug( uint32_t value ) {
    return( _debug_flag & value ? true : false );
}

bool App::nodelete( void ) {
    return( _nodelete );
}

void App::setNoDelete( const char *param ) {
    _nodelete = true;
}

App::~App() {

    if ( parser.defined("state") ) {

        /* generate the JSON data */

        Json::Value root;

        root["exit"] = _rc;
        root["time"]["start"] = _start;
        root["time"]["end"] = time( NULL );

        root["counts"]["errors"] = _errors;
        root["counts"]["alerts"] = _alerts;
        root["counts"]["infos"] = _infos;
        root["counts"]["debugs"] = _debugs;

        root["messages"] = Json::arrayValue;

        for ( string_list_i i = _messages.begin() ; i != _messages.end() ; i ++ ) {
            root["messages"].append( *i );
        }

        Json::StreamWriterBuilder wbuilder;
        wbuilder["indentation"] = "\t";
        std::string document = Json::writeString(wbuilder, root);

        /* and write it to a file */

        const char *filename = parser["state"];

        size_t len = strlen( filename );
        size_t index = len;

        std::string dirname;
        std::string basename;

        while (( index > 0 )&&( filename[ index - 1 ] != '/' )) {
            index --;
        }

        if ( index == 0 ) {
            /* no directory */
            dirname = "./";
            basename = filename;
        } else {
            dirname.assign( filename, 0, index );
            basename = &filename[ index ];
        }

        char *tmp_template = mprintf("%s.%s.XXXXXX",
                dirname.c_str(),
                basename.c_str() );

        int fd;

        if (( fd = mkostemp( tmp_template, 0644 )) >= 0 ) {

            write( fd, document.c_str(), document.size() );

            close( fd );

            if ( rename( tmp_template, filename ) < 0 ) {
                perror("rename");
            }

        }

        mfree( tmp_template );

    }

}

/* END OF FILE */
