/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-tres.h"

#define VALUE_LEN 128

DataTRES::DataTRES( const char *field ) : Data( field, DATATYPE_TRES ) {
    _type_count = 0;
    _types = NULL;
    _matching = NULL;
    _values = NULL;
    _set_value = NULL;
    _translate = NULL;
}

Data *DataTRES::make( const char *field, const YAML::Node &config ) {
    return( new DataTRES( field ) );
}

void DataTRES::_clear( void ) {

    for ( int ctr = 0 ; ctr < _type_count ; ctr ++ ) {
        _values[ ctr ][ 0 ] = '\0';
        _set_value[ ctr ] = false;
    }

}

int DataTRES::connect( const char *subtype ) {

    _connections ++;

    for ( int ctr = 0 ; ctr < _type_count ; ctr ++ ) {
        if ( strcasecmp( _types[ ctr ], subtype ) == 0 ) {
            return( ctr );
        }
    }

    /* extend _types, _values and _translate */

    int curr = _type_count;

    _type_count ++;

    _types = (char**)realloc( _types, sizeof( char * ) * _type_count );
    _matching = (size_t*)realloc( _matching, sizeof( size_t ) * _type_count );
    _translate = (bool*)realloc( _translate, sizeof( bool ) * _type_count );
    _values = (char**)realloc( _values, sizeof( char * ) * _type_count );
    _set_value = (bool*)realloc( _set_value, sizeof( bool ) * _type_count );

    size_t len = strlen( subtype );

    _types[ curr ] = strdup( subtype );
    _matching[ curr ] = ( subtype[ len - 1 ] == '*' ? len - 1 : 0 );
    _translate[ curr ] = strncasecmp( subtype, "mem", 3 ) == 0 ? true : false;
    _values[ curr ] = (char*)calloc( sizeof( char ), VALUE_LEN );
    _values[ curr ][ 0 ] = '\0';
    _set_value[ curr ] = false;

    return( curr );
}

int DataTRES::parse( const char *text, size_t len ) {

    while ( len > 0 ) {

        /* look for a , or \0, keeping track if an '=' is seen */

        size_t equal = 0;
        size_t comma = 0;

        for ( ; comma < len ; comma ++ ) {

            if ( text[ comma ] == '=' ) {
                equal = comma;
            } else if ( text[ comma ] == ',' ) {
                break;
            }

        }

        if ( equal == 0 ) {
            /* found = at an invalid location */
            throw( 808 );
        }

        /* hunt for a key match */

        for ( int ctr = 0 ; ctr < _type_count ; ctr ++ ) {

            if ( _matching[ ctr ] > 0 ) {

                size_t mlen = ( equal < _matching[ ctr ] ? equal : _matching[ ctr ] );

                if ( strncasecmp( _types[ ctr ], text, mlen ) != 0 ) {
                    /* doesn't match - try again */
                    continue;
                }

                const char *ptr = &text[ mlen ];

                for ( ; *ptr != '=' ; ptr ++ ) {
                    if ( ! *ptr ) {
                        /* abort - end of string */
                        continue;
                    }
                }

                ptr ++;

                if ( ! *ptr ) {
                    /* abort - end of string */
                    continue;
                }

                size_t value = atoi( ptr );

                if ( _values[ ctr ][0] != '\0' ) {
                    value += atoi( _values[ ctr ] );
                }

                snprintf( _values[ ctr ], sizeof( VALUE_LEN ), "%ld", value );
                _set_value[ ctr ] = true;

            } else if ( strncasecmp( _types[ ctr ], text, equal ) == 0 ) {
                /* found an match exact! */

                if ( _translate[ ctr ] ) {
                    snprintf( _values[ ctr ], VALUE_LEN, "%ld", toMBytes( &text[ equal + 1 ], comma - equal - 1 ) );
                } else {
                    size_t value_len = comma - equal - 1;

                    strncpy( _values[ ctr ], &text[ equal + 1 ], value_len );
                    _values[ ctr ][ value_len ] = '\0';
                }
                _set_value[ ctr ] = true;
                break;
            }

        }

        if ( comma == len ) {
            break;
        }

        comma ++;

        text += comma;
        len -= comma;

    }

    _set = true; 

    return( 0 );
}

const char *DataTRES::value( int index ) {

    if (( index < 0 )||( index >= _type_count )) {
        throw( AppExcept( "DataTRES index %d is out of range (max %d)", index, _type_count ));
    } else if ( ! _set_value[ index ] ) {
        return( NULL );
    } else {
        return( _values[ index ] );
    }

}

/* END OF FILE */
