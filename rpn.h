/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _RPN_H_
#define _RPN_H_

#include    "mylib.h"

#define RPN_STACK_SIZE      100

typedef double rpn_t;

class RPN {

    public:

        RPN();
        ~RPN();

        void compile( const char *text );
        
        bool defined( void );

        rpn_t run( rpn_t value );

    protected:

        struct {
            rpn_t       value;
            void        (RPN::*op)();
        } _estack[ RPN_STACK_SIZE ];

        int     _estack_size;

        rpn_t   _stack[ RPN_STACK_SIZE ];
        int     _stack_size;

    private:

        void    op_add( void );
        void    op_subtract( void );
        void    op_multiply( void );
        void    op_divide( void );

        rpn_t pop( void );
        void push( rpn_t value );



};

#endif
