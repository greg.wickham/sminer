/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "mylib.h"
#include    "stats.h"

Stats table_node( "node" );
Stats table_job( "job" );
Stats table_job_link( "job_link" );
Stats table_step( "step" );
Stats table_step_link( "step_link" );
Stats table_user( "user" );

Stats::Stats( const char *name ) {
    _name = xstrdup( name );
    clear();
}

void Stats::dump( void ) {

    printf("%12s %6d insert / %6d update / %6d duplicate / %6d error / %6d discard / %6d clean / %6d deleted",
            _name, insert, update, duplicate, error, discard, clean, deleted );

}

/* END OF FILE */
