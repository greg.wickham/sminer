/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _SACCT_HEADER_H_
#define _SACCT_HEADER_H_

#include    "mylib.h"

class Data;
class DataJobID;
class DataDateTime;
class DataTRES;
class DataNodeList;
class DataElapsedRaw;

class Parser {

    public:

        Parser();
        ~Parser();

        void label( const char *text, size_t len );
        void validate( void );
        void reset( void );
        bool isset( void );

        void data( const char *text, size_t len );

        void clear( void );

        const char *job( void );

        bool ran( void );
        bool invalidTime( time_t epoch_start, time_t epoch_end );

#if 0
        bool shortJob( void );
#endif

        void line( const char *line, void (Parser::*cb)( const char *text, size_t len ) );

        void debug( int value );

        DataNodeList *nodelist( void );

        const char *job_id( void );

    private:

        findex_t    _nfields;
        Data        **_map;

        findex_t    _index;

        DataJobID       *_job_id;
        Data            *_job_id_base;
        DataDateTime    *_time_submit;
        DataDateTime    *_time_start;
        DataDateTime    *_time_end;
        DataTRES        *_alloc_tres;
        DataNodeList    *_node_list;
        DataElapsedRaw  *_elapsed_raw;

        int             _debug;

};

#endif
