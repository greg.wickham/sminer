/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-elapsed-raw.h"

#undef DEBUG

DataElapsedRaw::DataElapsedRaw( const char *field ) : Data( field, DATATYPE_ELAPSED_RAW ) {
    
}

void DataElapsedRaw::_clear( void ) {

}

Data *DataElapsedRaw::make( const char *field, const YAML::Node &config ) {
    return( new DataElapsedRaw( field ) );
}

int DataElapsedRaw::parse( const char *text, size_t len  ) {

    assert( len > 0 && "DataElapsedRaw::parse len == 0" );

    if ( *text == '0' ) {
        /* ignore the value - 0 seconds */
        return( 0 );
    }

    for ( size_t ctr = 0 ; ctr < len ; ctr ++ ) {
        if ( ! isdigit( text[ ctr ] ) ) {
            std::string tmp( text, len );
            throw( AppWarning( "%s: unexpected character found '%c'\n", _field, text[ ctr ], tmp.c_str() ) );
        }
    }

    _value.assign( text, len );
    _set = true;

    return( 0 );
}

/* END OF FILE */
