/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#undef DEBUG_JOB

#include    "sacct.h"
#include    "mylib.h"
#include    "timer.h"
#include    "data-jobid.h"
#include    "data-datetime.h"
#include    "data-nodelist.h"
#include    "data-tres.h"
#include    "query.h"
#include    "postgres.h"
#include    "db-table.h"
#include    "stats.h"
#include    "node.h"
#include    "config.h"
#include    "slurm.h"
#include    "app.h"
#include    "config.h"

/* 'scan' the last <n> days */
time_t   SACCT::scan = 0;

/* scan interval is <n> days */
time_t   SACCT::duration = 1;

/* start time of scan */
time_t   SACCT::timeStart = 0;

/* end time of scan - if not set use 'time()' */
time_t   SACCT::timeEnd = 0;

SACCT::SACCT( Config &config )  {
    _period_begin = 0;
    _period_end = 0;

    _sacct_cmd = "sacct";

    config.set_callback( sigc::mem_fun( *this, &SACCT::configure ) );

}

void SACCT::configure( YAML::Node &data ) {

    YAML::Node tmp = data["sacct_path"];

    if ( tmp.IsDefined() ) {

        if ( ! tmp.IsScalar() ) {
            throw( AppFatal("'sacct_path' must be a scalar" ) );
        }

        _sacct_cmd = tmp.as<std::string>();

    }

}

int SACCT::run( void ) {

    Timer run;

    run.start();

    _job = DB::Table::fetch( "job" );
    //_job->debug( true );
    _step = DB::Table::fetch( "step" );
    //_step->debug( true );
    _step_node_link = DB::Table::fetch( "step_node_link" );
    //_step_node_link->debug( true );
    _job_node_link = DB::Table::fetch( "job_node_link" );
    //_job_node_link->debug( true );
    _node = DB::Table::fetch( "node" );
    if ( _node ) {
        _node->lock( true );
        //_node->debug( true );
        _node->preload();
    }

    struct tm tm;
    bzero( &tm, sizeof( tm ));

    tm.tm_year = 118;
    tm.tm_mon = 0;
    tm.tm_mday = 1;

    /*
    tm.tm_year = 121;
    tm.tm_mon = 0;
    tm.tm_mday = 31;
    */

    if ( timeStart && scan ) {
        throw( AppFatal("Only one of '--start <t>' or '--scan <d>' can be specified. Use '--help' for more information" ) );
    }

    if ( ! timeStart ) {
        timeStart = rewindToMidnight(  time(NULL) - (( scan -1 ) * 86400 ) );
    }

    time_t interval = duration * 86400;

    _period_begin = timeStart;

    for ( ; true ; _period_begin += interval ) {

        _discard_time = 0;

        if ( timeEnd && ( _period_begin >= timeEnd ) ) {
            /* an end time is specified, and it has been exceeded */
            break;
        } else if ( _period_begin >= time( NULL ) ) {
            /* no end time, so break when exceeding the current time */
            break;
        }

        _period_end = _period_begin + interval;

        struct tm *tm = localtime( &_period_begin );

        _time_epoch_start = _period_begin;
        _time_epoch_end = _period_end;

        snprintf( _time_text_start, sizeof( _time_text_start ), "%04d-%02d-%02dT00:00:00",
                tm->tm_year + 1900, 
                tm->tm_mon + 1,
                tm->tm_mday );

        tm = localtime( &_period_end );

        snprintf( _time_text_end, sizeof( _time_text_end ), "%04d-%02d-%02dT00:00:00",
                tm->tm_year + 1900, 
                tm->tm_mon + 1,
                tm->tm_mday );

        app.info("processing period %s to %s", _time_text_start, _time_text_end );

        _job->preload( _time_text_start, _time_text_end );
        if ( _job_node_link ) { 
            _job_node_link->preload( _time_text_start, _time_text_end, _job );
        }
        if ( _step ) {

            _step->preload( _time_text_start, _time_text_end );

            if ( _step_node_link ) { 
                _step_node_link->preload( _time_text_start, _time_text_end );
            }
        }

        _job_node_link = DB::Table::fetch( "job_node_link" );

#ifdef DEBUG_JOB_X
        char *cmd = mprintf("%s -P%s --noconvert -j 36190808 -D --state CD,R,CA,F,NF,RQ,TO,S --allusers --format=%s",
                _sacct_cmd.c_str(),
                _step ? "" : " -X",
                Slurm::sacct_fields() );
#else
        char *cmd = mprintf("%s -P%s --noconvert -S %s -E %s -D --allusers --state CD,R,CA,F,NF,RQ,TO,S --format=%s",
                _sacct_cmd.c_str(),
                _step ? "" : " -X",
                _time_text_start, _time_text_end,
                Slurm::sacct_fields() );
#endif

#ifdef DEBUG_JOB
        printf("CMD = %s\n", cmd );
#endif

        app.debug("cmd = %s", cmd );
        app.info("executing \"sacct -S %s -E %s ...\"", _time_text_start, _time_text_end );

        FILE *fp;

        Timer timer;

        timer.start();

        Timer first;

        first.start();

        if (( fp = popen( cmd, "r" )) == NULL ) {
            throw( AppFatal("sacct popen: %s", strerror( errno )));
        }

        char line[ 65536 ];
        int line_ctr = 0;

        _job_index = NULL;
        _never_ran = 0;

        while ( fgets( line, sizeof( line ), fp ) ) {

            if ( first.running() ) {
                first.stop();
                app.info("first sacct record available in %.2lf seconds", first.seconds() );
            }

            line_ctr ++;
            size_t len = strlen( line );
            while (( len > 0 )&&( isspace( line[ len - 1 ] ))) {
                line[ -- len ] = '\0';
            }

#ifdef DEBUG_JOB
            printf("LINE %s\n", line );
#endif

            if ( ! _parser.isset() ) {
                /* process header */

                _parser.reset();

                try {
                    _parser.line( line, &Parser::label );
                    _parser.validate();
                } catch ( AppExcept &e ) {
                    app.error("sacct[%d]: %s", line_ctr, line );
                    app.error("sacct[%d]: %s", line_ctr, e.what() );
                    throw( AppFatal( "sacct error - check log for details" ) );
                }

            } else {
                /* process data */

                _parser.clear();

                /* and parse the data line */
                try {

                    _parser.line( line, &Parser::data );

                    if ( ! _parser.job_id() ) {
                        printf("LINE %s\n", line );
                        printf("JOB ID is NULL\n");
                        exit( 1 );
#ifdef XDEBUG_JOB
                    } else if ( strcmp( _parser.job_id(), "26815402" ) != 0 ) {
                        /* debugging specific job */
                        continue;
                    } else {
                        printf("DEBUG %s\n", line );
#endif
                    }

                    if ( ! _parser.ran() ) {

                        /* job never consumed any resources */
                        _never_ran ++;

                    } else if ( _parser.invalidTime( _time_epoch_start, _time_epoch_end ) ) {

                        _discard_time ++;

                    } else if ( ! _parser.job() ) {

                        /* process on the job table */

                        _job->inject();
                        _job_node_link->inject( _job );

#ifdef DEBUG_JOB
                        //exit(1);
#endif
                    } else if ( _step ) {

                        _step->inject();
                        if ( _step_node_link ) {
                            _step_node_link->inject();
                        }

                    }

                } catch ( AppExcept &e ) {
                    app.error("sacct[%d]: %s", line_ctr, line );
                    app.error("sacct[%d]: %s", line_ctr, e.what() );
                    throw( AppFatal( "sacct error - check log for details" ) );
                } catch ( AppWarning &e ) {
                    app.alert("sacct[%d]: %s", line_ctr, line );
                    app.alert("sacct[%d]: %s", line_ctr, e.what() );
                } catch ( AppFatal &e ) {
                    app.error("sacct[%d]: %s", line_ctr, line );
                    app.error("sacct[%d]: %s", line_ctr, e.what() );
                    throw( AppFatal( "sacct error - check log for details" ) );
                }
            }

        }

        /* remove unused rows from the job node link table */

        if ( _step_node_link ) {
            _step_node_link->purge();
        }

        _job_node_link->purge();

        /* reset the parser state, ready for next time */
        _parser.reset();

        if ( pclose( fp ) < 0 ) {
            app.error( "failure closing pipe: %s", strerror( errno ));
            throw( AppFatal( "sacct error - check log for details" ) );
        }

        timer.stop();

        app.info("  %6d lines in %11.3lf seconds (%9.2lf rps) %ld never ran, %d discarded",
                line_ctr,
                timer.seconds(),
                (double)line_ctr / timer.seconds(), 
                _never_ran,
                _discard_time );

        DB::Table::stats();
        DB::Table::clear();

        mfree( cmd );

    }

    run.stop();

    app.info("End of run in %.6lf seconds", run.seconds() );

    return( 0 );
}

/*
 * FUNCTION:    setDayCount
 *
 * PARAMS:      const char * - days to cover
 *
 * RETURNS:     none
 *
 * THROWS:      AppParam if not an integer
 */
void SACCT::setDayCount( const char *value, void *ptr ) {

    for ( const char *ptr = value ; *ptr ; ptr ++ ) {
        if ( ! isdigit( *ptr ) ) {
            throw( AppParam("value '%s' is invalid - must only contain digits", value ) );
        }
    }

    time_t number = atoi( value );

    if ( number <= 0 ) {
        throw( AppParam("%d is too small - day count must be > 0", number ) );
    } else if ( number > 31 ) {
        throw( AppParam("%d is too large - day count be <= 31", number ) );
    }

    /* store the day count */
    *(time_t*)ptr = number;

}

/*
 * FUNCTION:    setTime
 *
 * PARAMS:      const char * - start timestamp
 *
 * RETURNS:     none
 *
 * THROWS:      AppParam if value not converted
 *
 * DESCRIPTION: Valid formats are:
 *
 *              YYYY-MM-DD
 */
void SACCT::setTime( const char *value, void *ptr ) {

    const char *formats[] = {
        "%Y-%m-%d",
        "%Y/%m/%d",
        NULL
    };

    for ( int index = 0 ; formats[ index ] ; index ++ ) {

        struct tm tm;
        bzero( &tm, sizeof( tm ));

        const char *rv = strptime( value, formats[ index ], &tm );

        if (( rv )&&( rv[0] == '\0' )) {
            *(time_t*)ptr = mktime( &tm );
            return;
        }

    }

    throw( AppParam("unable to convert '%s' into a timestamp", value ) );

}

/* END OF FILE */
