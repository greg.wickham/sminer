/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-state.h"

DataState::DataState( const char *field ) : Data( field, DATATYPE_STATE ) {
    _state = NULL;
    _reason = NULL;
}

Data *DataState::make( const char *field, const YAML::Node &config ) {
    return( new DataState( field ) );
}

void DataState::_clear( void ) {
    mfree( _state );
    mfree( _reason );
}

int DataState::parse( const char *text, size_t len ) {

    mfree( _state );
    mfree( _reason );

    size_t offset;

    for ( offset = 0 ; ( offset < len )&&( text[ offset ] != ' ' ) ; offset ++ ) {
        /* spin */
    }

    _state = xstrndup( text, offset );

    /* if a reason exists, then save it too! */
    if ( text[ offset ] == ' ' ) {
        offset ++;
        _reason = xstrndup( &text[ offset ], len - offset );
    }

    _set = true;

    return( 0 );
}

int DataState::connect( const char *subtype ) {
    _connections ++;
    if ( subtype == NULL ) {
        return( 0 );
    } else if ( strcasecmp( subtype, "0" ) != 0 ) {
        return( 1 );
    } else {
        return( 0 );
    }
}

const char *DataState::value( int index ) {

    if ( _set ) {
        if ( index == 0 ) {
            return( _state );
        } else {
            return( _reason );
        }
    } else {
        return( NULL );
    }

}

/* END OF FILE */
