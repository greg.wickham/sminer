/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _APP_H_
#define _APP_H_

#include    "mylib.h"

typedef struct {
    int         priority;
    const char  *text;
    const char  *desc;
} LOG_DEFN;

class Args;

class App {

    public:

        App();
        ~App();

        void configure( Args &parser );

        /* convenience wrappers for the 'log' methods */
        void error( const char *fmt, ... );
        void info( const char *fmt, ... );
        void alert( const char *fmt, ... );
        void debug( const char *fmt, ... );

        void summary( void );

        void log( int priority, const char *fmt, ... );
        void log( int priority, const char *fmt, va_list ap );

        bool    verbose;
        bool    en_stdout;
        bool    updates;

        bool    saveRunning;

        void configure( YAML::Node map );

        static LOG_DEFN log_levels[];

        const char *program;

        void exit( int rc );

        void setDebug( const char *param );
        void setNoDelete( const char *param );
        bool debug( uint32_t value );
        bool nodelete( void );

    private:

        time_t      _start;
        time_t      _time;
        char        _time_text[ 64 ];
        bool        _syslog;

        FILE        *_fp_log;
        char        *_fp_log_filename;
        bool        _use_stdout;

        int         _priority;

        int         _rc;

        uint32_t    _debug_flag;
        bool        _nodelete;

        /* counters of message types */

        int         _errors;
        int         _alerts;
        int         _infos;
        int         _debugs;

        string_list_t   _messages;

};

extern App app;

#endif
