/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "args.h"

Args::Args() {
    _basename = NULL;
    _desc = NULL;
}

Option &Args::addRequiredArgument( const char *key, char ch ) {
    Option *option = new Option( *this, key, ch, required_argument );
    _options.insert( Option_pair_t( key, option ) );
    return( *option );
}

Option &Args::addArgument( const char *key, char ch ) {
    Option *option = new Option( *this, key, ch, no_argument );
    _options.insert( Option_pair_t( key, option ) );
    return( *option );
}

void Args::parse_args( int argc, char *argv[] ) {

    _basename = ::basename( argv[ 0 ] );

    addArgument( "help", 'h' )
        .desc("Display this help text")
        .thenExit( true )
        .call( &Args::help );

    addArgument( "version", 'v' )
        .desc("Display version")
        .thenExit( true )
        .call( &Args::version );

    struct option *long_options =
        (struct option *)calloc( sizeof( struct option ), ( _options.size() + 1 ) );

    std::string     chars;
    int             index;
    Option_map_i    i;

    for ( index = 0, i = _options.begin() ; i != _options.end() ; i ++, index ++ ) {
        struct option &option = long_options[ index ];
        Option *ptr = i->second;
        ptr->_index = index + 1;
        option.name = xstrdup( ptr->_long );
        option.has_arg = ptr->_has_arg;
        option.flag = NULL;
        option.val = ptr->_index;

        if ( isprint( ptr->_short )&&( ! isspace( ptr->_short ))) {
            chars += ptr->_short;
            if ( ptr->_has_arg == required_argument ) {
                chars += ':';
            }
        }

    }

    /* end the list */

    struct option &option = long_options[ index ];
    option.name = NULL;
    option.has_arg = 0;
    option.flag = NULL;
    option.val = 0;

    /*  */

    while ( 1 ) {

        int option_index;
        int c;
 
        if (( c = getopt_long(
                        argc,
                        argv,
                        chars.c_str(),
                        long_options, &option_index)) < 0 ) {
            break;
        } else if ( c == '?' ) {

            fprintf( stderr, "%s: unknown parameter encountered. Use '--help' for assistance\n", argv[ 0 ] );
            exit( 1 );

        }

        Option_map_i    i;
        int             index;

        for ( index = 0, i = _options.begin() ; i != _options.end() ; i ++, index ++ ) {

            if (( c == long_options[ index ].val )||( c == i->second->_short )) {
                i->second->put( optarg );
            }

        }

    }

}

bool Args::defined( const char *key ) {

    for ( Option_map_i i = _options.begin() ; i != _options.end() ; i ++ ) {
        if ( strcmp( key, i->second->_long ) == 0 ) {
            return( i->second->isset() );
        }
    }

    throw( AppFatal("ArgParser: key '%s' is not valid", key ) );

}

const char * Args::operator[]( const char *key ) {


    for ( Option_map_i i = _options.begin() ; i != _options.end() ; i ++ ) {
        if ( strcmp( key, i->second->_long ) == 0 ) {
            return( i->second->value() );
        }
    }

    throw( AppFatal("ArgParser: key '%s' is not valid", key ) );

}

const char *Args::basename( void ) {
    return( _basename );
}

void Args::help( void ) {

    if ( _desc ) {
        printf("%s\n", _desc );
    }

    printf("\n");

    printf("Usage:\n");

    printf("\n  %s", basename()) ;

    size_t len = 0;

    for ( Option_map_i i = _options.begin() ; i != _options.end() ; i ++ ) {

        Option &opt = *(i->second);

        std::string output = " [ --" + i->first;

        if ( opt._short ) {
            output += " | -";
            output += opt._short;
        }

        if ( opt._has_arg == required_argument ) {
            output += " <parameter>";
        }
        output += " ]";

        if ( len > 80 ) {
            printf(" \\\n\t" );
            len = 8;
        }

        printf("%s", output.c_str() );
        len += output.size();

    }

    printf("\n\n");

    printf("Options:\n");

    for ( Option_map_i i = _options.begin() ; i != _options.end() ; i ++ ) {
        Option &opt = *(i->second);
        printf("\n");
        printf("    --%s", i->first.c_str() );

        if (  opt._short ) {
            printf(" | -%c", opt._short );
        }

        if ( opt._has_arg == required_argument ) {
            printf(" <parameter>");
        }
        printf("\n");
        printf("\t%s\n", opt._desc );

    }

    printf("\n");

    if ( PROG_GIT ) {
        printf("Report bugs at %s\n", PROG_GIT );
    }

}

void Args::version( void ) {
    printf("%s: %s\n", _basename, PROG_VERSION );
}

void Args::desc( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    mfree( _desc );
    _desc = vmprintf( fmt, ap );
    va_end( ap );
}

/* ---- */

/*
 * METHOD:      set
 *
 * PARAMETER:   const char * - value
 *
 * RETURNS:     void
 *
 * THROWS:      AppFatal on error
 *
 * WHATITDOES:  Invoked from Args::parse_args
 *              when the long or short is encountered
 *              during argument procesing.
 */
void Option::put( const char *value ) {

    _set = true;

    /* keep a copy of the option value */
    mfree( _value );
    _value = xstrdup( value );

    /* if a callback handler is defined, then invoke it */

    try {
        if ( _handler_void_cb ) {
            (*_handler_void_cb)( value, _handler_void_ptr );
            return;
        }
    } catch ( AppParam &e ) {
        throw( AppFatal( "error parsing parameter \"--%s\": %s\n", _long, e.what() ) );
    }

    if ( ! _callback_signal.empty() ) {
        _callback_signal( value );
    }

    switch ( _store_type ) {

        case OT_INTEGER:
            *((int*)_store_ptr) = atoi( value );
            return;

        case OT_BOOLEAN:
            *((bool*)_store_ptr) = value ? booleanFromString( value ) : true;
            return;

        case OT_STRING:
            *((char**)_store_ptr) = xstrdup( value );
            return;

        case OT_CALLBACK:
            _store_cb( value );
            return;

        default:
            break;
    }

    /* external action callback - prototype 'void cb( void )'
     *   invoke it immediately if defined 
     */
    if ( _action_void_cb ) {
        (*_action_void_cb)();
    }

    /* internal action callback - invoke a method of the Arg class
     * Used for '--help'.
     * */
    if ( _action ) {
        (_args.*_action)();
    }

    /* if the exit flag is defined then stop immediately */
    if ( _exit ) {
        exit( 0 );
    }

}

Option::Option( Args &args, const char *key, char ch, int has_arg ) : _args( args ) {
    _short = ch;
    _long = xstrdup( key );
    _has_arg = has_arg;
    _value = NULL;
    _desc = NULL;
    _set = false;

    _index = 0;
    _action = NULL;
    _action_void_cb = NULL;
    _exit = false;
    _handler_void_cb = NULL;
    _handler_void_ptr =  NULL;

    _default_ptr = NULL;
    _default_type = OT_NONE;

    _store_type = OT_NONE;
    _store_ptr = NULL;

}

Option &Option::value( const char *value ) {

    _default_type = OT_STRING;
    _default_ptr = xstrdup( value );

    return( *this );
}

Option &Option::value( bool value ) {
    return( *this );
}

Option &Option::thenExit( bool value ) {
    _exit = value;
    return( *this );
}

Option &Option::call( callback_slot_t cb ) {
    _callback_signal = cb;
    return( *this );
}

/* Define an arbitrary 'void cb( void )' to call
 * if this option is encountered.
 */
Option &Option::call( action_cb_t cb ) {
    _action = cb;
    return( *this );
}

/*
 * Define an internall call (Args::*cb)() 
 * ie: mainly used for '--help'
 */
Option &Option::call( action_void_cb_t cb ) {
    _action_void_cb = cb;
    return( *this );
}

/* define the help description of this parameter */
Option &Option::desc( const char *value ) {
    mfree( _desc );
    _desc = xstrdup( value );
    return( *this );
}

/* define the callback that includes a void * parameter */
Option &Option::call( action_set_void_t cb, void *ptr ) {
    _handler_void_cb = cb;
    _handler_void_ptr = ptr;
    return( *this );
}

/* can be invoked to obtain the encountered value */
const char *Option::value( void ) {
    if ( _value ) {
        return( _value );
    } else if ( _default_type == OT_STRING ) {
        return( (char*)_default_ptr );
    } else {
        return( NULL );
    }
}

#if 1
/* directly update the BOOLEAN indicated by this pointer */
Option &Option::update( bool *ptr ) {
    _store_type = OT_BOOLEAN;
    _store_ptr = ptr;
    return( *this );
}

/* directly update the CHAR* indicated by this pointer */
Option &Option::update( char *ptr ) {
    _store_type = OT_STRING;
    _store_ptr = ptr;
    return( *this );
}

/* directly update the INTEGER indicated by this pointer */
Option &Option::update( int *ptr ) {
    _store_type = OT_INTEGER;
    _store_ptr = ptr;
    return( *this );
}
#endif

Option &Option::update( callback_slot_t cb ) {
    _store_type = OT_CALLBACK;
    _store_cb = cb;
    return( *this );
}

bool Option::isset( void ) {
    return( _set );
}

/* END OF FILE */
