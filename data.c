/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data.h"
#include    "data-state.h"
#include    "data-string.h"
#include    "data-elapsed.h"
#include    "data-elapsed-raw.h"
#include    "data-datetime.h"
#include    "data-int.h"
#include    "data-node.h"
#include    "data-nodelist.h"
#include    "data-jobid.h"
#include    "data-tres.h"
#include    "data-int.h"
#include    "data-bytes.h"
#include    "app.h"

Data     **Data::fields = NULL;
size_t   Data::fields_count = 0;

Data::Data( const char *field, DataType type ) {
    _type = type;
    _field = xstrdup( field );
    _set = false;
    _connections = 0;
}

const char *Data::name( void ) {
    return( _field );
}

DataType Data::type( void ) {
    return( _type );
}

void Data::clear( void ) {
    _set = false;
    _value.clear();
    this->_clear();
};

int Data::connect( const char *subtype ) {
    /* no subtype by default */
    _connections ++;
    return( 0 );
}

const char *Data::begin( void ) {
    return( NULL );
}

const char *Data::next( void ) {
    return( NULL );
}

bool Data::iterator( void ) {
    return( false );
}

#if 0
void Data::configure( const YAML::Node &node ) {
    /* by default, do nothing */
}
#endif

void Data::configure( const YAML::Node map ) {

    static struct {
        const char  *name;
        Data        *(*factory)( const char *fname, const YAML::Node &config );
    } dtypes[] = {
        { "DateTime",   &DataDateTime::make },
        { "Elapsed",    &DataElapsed::make },
        { "ElapsedRaw", &DataElapsedRaw::make },
        { "Integer",    &DataInt::make },
        { "JobID",      &DataJobID::make },
        { "Node",       &DataNode::make },
        { "NodeList",   &DataNodeList::make },
        { "State",      &DataState::make },
        { "String",     &DataString::make },
        { "TRES",       &DataTRES::make },
        { "Bytes",      &DataBytes::make },
        { NULL,         NULL }
    };

    if ( map.Type() != YAML::NodeType::Map ) {
        throw( AppExcept("expeced YAML Map" ) );
    }

    fields_count = map.size();
    fields = (Data**)calloc( fields_count, sizeof( Data *) );

    int index = 0;

    for ( YAML::const_iterator i = map.begin() ; i != map.end() ; i ++, index ++ ) {

        /* a quick handle to the name of the field */

        const char *name = i->first.as<std::string>().c_str();

        /* determine the field type */

        std::string data_type;

        YAML::Node  config;

        const char      *type = NULL;

        if ( i->second.IsScalar() ) {

            type = i->second.as<std::string>().c_str();

        }  else if ( i->second.Type() == YAML::NodeType::Map ) {

            config = i->second;

            if ( config["type"].IsScalar() ) {
                type = config["type"].as<std::string>().c_str();
            } else {
                throw( AppFatal("%s: bad YAML node - missing 'type'", name ) );
             }

        } else {
            throw( AppFatal("%s: bad YAML node - must be Salar or Map", name ) );
        }

        /* construct using the Data type */

        int ctr = 0;

        for ( ctr = 0 ; dtypes[ ctr ].name ; ctr ++ ) {

            if ( strcasecmp( dtypes[ ctr ].name, type ) == 0 ) {
                try {
                    fields[ index ] = dtypes[ ctr ].factory( name, config );
                } catch ( AppFatal &e ) {
                    throw( AppFatal( "%s: %s", name, e.what() ) );
                }
                break;
            }

        }

        if ( ! dtypes[ ctr ].name ) {
            throw( AppFatal("%s: type '%s' is not valid", name, type ) );
        }

    }

    if ( app.verbose ) {
        app.info("configured %ld sacct fields", fields_count );
    }

}

Data *Data::connect( const char *type, int *index ) {

    char  *main = NULL;
    char  *subtype = NULL;
    const char *colon;

    if (( colon = strchr( type, ':' )) == NULL ) {
        main = strdup( type );
    } else {
        main = strndup( type, colon - type );
        subtype = strdup( &type[ colon - type + 1 ] );
    }

    Data *match = NULL;

    for ( size_t index = 0 ; index < fields_count ; index ++ ) {

        Data *data = fields[ index ];

        if ( strcasecmp( data->name(), main ) == 0 ) {
            match = data;
            break;
        }

    }

    if ( ! match ) {
        throw( AppFatal( "sacct field '%s' not found", main ) );
    } else if ( match ) {
        *index = match->connect( subtype );
    } else {
        *index = 0;
    }

    mfree( main );
    mfree( subtype );
    return( match );
}

/* END OF FILE */
