/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/
#ifndef _POSTGRES_H_
#define _POSTGRES_H_

#include    "mylib.h"

#include    <libpq-fe.h>

class Query;

class Postgres {

    public:

        Postgres();

        void configure( const YAML::Node &config );

        void connect( void );

        bool errorContains( const char *text );

        int execute( Query &query );

        int resultStatus( void );
        int ntuples( void );
        char *fname( int column_number);
        int nfields( void );
        char *getvalue( int row_number, int column_number );
        int getisnull( int row_number, int column_number );
        int getlength( int row_number, int column_number );
        const char *errorMessage( void );
        char *resultErrorField( int fieldcode );

    protected:

    private:

        PGconn      *_conn;
        PGresult    *_results;

        /* configuration from file */
        YAML::Node  _config;

};

extern Postgres pg;

#endif
