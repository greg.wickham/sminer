/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _SACCT_H_
#define _SACCT_H_

#include "mylib.h"

#include    "db-table.h"
#include    "parser.h"

class DataJobID;
class DataTRES;
class DataDateTime;
class DataNodeList;
class Config;

class SACCT {

    public:

        SACCT( Config &config );

        int run( void );

        static void setTime( const char *value, void *ptr );
        static void setDayCount( const char *value, void *ptr );

        static time_t   duration;
        static time_t   scan;
        static time_t   timeStart;
        static time_t   timeEnd;

    private:

        void configure( YAML::Node &data );

        time_t  _time;

        char    _time_text_start[ 64 ];
        time_t  _time_epoch_start;
        char    _time_text_end[ 64 ];
        time_t  _time_epoch_end;

        /* period being scanned */
        time_t      _period_begin;
        time_t      _period_end;

        size_t      _never_ran;

        DB::Table *_job;
        DB::Table *_step;
        DB::Table *_step_node_link;
        DB::Table *_job_node_link;
        DB::Table *_node;

        findex_t        _step_job_index_id;
        findex_t        _job_index_id;
        const char      *_job_index;

        size_t          _discard_time;

        Parser          _parser;

        std::string     _sacct_cmd;

};

#endif
