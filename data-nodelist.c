/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-nodelist.h"

DataNodeList::DataNodeList( const char *field ) : Data( field, DATATYPE_NODELIST ) {
    _last = NULL;
}

Data *DataNodeList::make( const char *field, const YAML::Node &config ) {
    return( new DataNodeList( field ) );
}

void DataNodeList::_clear( void ) {
    _nodes.clear();
    _iter = _nodes.end();
    _last = NULL;
}

int DataNodeList::parse( const char *text, size_t len ) {

    if (( len == 13 )&&( strncmp( text, "None assigned", len ) == 0 )) {
        /* "None assigned" sometimes happens . . */
        return( 0 );
    }

    std::string field( text, len );

    try {
        nodeExpand( _nodes, field );
    } catch ( int e ) {
        throw( AppExcept( "parsing %s: %d", field.c_str(), e ) );
    }

    _set = true;
    _iter = _nodes.begin();

    return( 0 );
}

string_list_t &DataNodeList::nodes( void ) {
    return( _nodes );
}

const char *DataNodeList::value( int index ) {

    if ( ! _last ) {
        _last = begin();
    }

    return( _last );
}

bool DataNodeList::iterator( void ) {
    return( true );
}

const char *DataNodeList::begin( void ) {
    _nodes.sort();
    _iter = _nodes.begin();
    return( _last = next() );
}

const char *DataNodeList::next( void ) {
    if ( _iter == _nodes.end() ) {
        return( _last = NULL );
    } else {
        const char *tmp = (*_iter ++).c_str();
        return( _last = tmp );
    }
}

/* END OF FILE */
