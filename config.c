/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "config.h"
#include    "db-table.h"
#include    "slurm.h"
#include    "data.h"
#include    "app.h"
#include    "postgres.h"

Config config;

Config::Config() {
}

void Config::set_callback( config_slot_t slot ) {
    _signal.connect( slot );
}

void Config::load( const char *filename ) {

    try {

        YAML::Node data = YAML::LoadFile( filename );

        if ( ! data.IsMap() ) {
            throw( AppExcept("Incorrectly formatted YAML file - expected a top level Map") );
        }

        try {
            app.configure( data["logging"] );
        } catch ( const AppFatal &e ) {
            throw( AppExcept( "log configuration: %s", e.what() ));
        }

        try {
            pg.configure( data["postgres"] );
        } catch ( YAML::BadConversion &e ) {
            throw( AppFatal( "postgres configuration: Invalid YAML format" ) );
        } catch ( const AppExcept &e ) {
            throw( AppFatal( "postgres configuration: %s", e.what() ));
        }

        try {
            Data::configure( data["sacct"] );
        } catch ( const AppExcept &e ) {
            throw( AppExcept( "sacct configuration: %s", e.what() ));
        } catch ( const AppFatal &e ) {
            throw( AppFatal( "sacct configuration: %s", e.what() ));
        }

        try { 
            DB::Table::configure( data["tables"] );
            DB::Table::resolve();
        } catch ( const AppExcept &e ) {
            throw( AppExcept( "table configuration: %s", e.what() ));
        }

        try {
            _signal.emit( data );
        } catch ( const AppExcept &e ) {
            throw( AppExcept( "configuration: %s", e.what() ));
        }

    } catch ( YAML::ParserException &e ) {

        throw( AppFatal( "%s: YAML parsing error: %s (line %d column %d)",
                filename, e.msg.c_str(),
                e.mark.line, e.mark.column ) );

    } catch ( YAML::BadFile &e ) {

        throw( AppFatal( "%s: Unable to read configuration - %s",
                filename, strerror( errno )));

    } catch ( YAML::BadSubscript &e ) {

        throw( AppFatal( "%s: Unable to load YAML file: %s",
                filename, e.msg.c_str() ));

    }

}

/* END OF FILE */
