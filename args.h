/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _ARGS_H_
#define _ARGS_H_

#include    "mylib.h"

class Args;

typedef void (*action_void_cb_t)( void );
typedef void (Args::*action_cb_t)( void );
typedef void (*action_set_void_t)( const char *param, void *ptr );

typedef enum {
    OT_NONE = 0,
    OT_STRING,
    OT_BOOLEAN,
    OT_INTEGER,
    OT_CALLBACK
} OptionType;

typedef sigc::signal<void,const char *> callback_signal_t;
typedef sigc::slot<void,const char *> callback_slot_t;

class Option {

    public:

        Option( Args &args, const char *key, char ch, int has_arg );
        Option &value( const char *value );
        Option &value( bool value );
        Option &desc( const char *value );
        Option &call( action_set_void_t cb, void *ptr = NULL );
        Option &thenExit( bool value);
        Option &call( action_void_cb_t cb );
        Option &call( action_cb_t cb );
        Option &call( callback_slot_t cb );

        Option &update( bool *ptr );
        Option &update( char *ptr );
        Option &update( int *ptr );
        Option &update( callback_slot_t cb );

#if 0
        Option &update( Setter<int> &ptr );
        Option &update( Setter<bool> &ptr );
        Option &update( Setter<std::string> &ptr );
#endif

        bool isset( void );

        const char *value( void );
        void put( const char *value );

        Args    &_args;
        int     _index;
        int     _has_arg;
        char    _short;
        char    *_long;
        char    *_desc;
        char    *_value;
        action_cb_t _action;
        bool    _exit;
        bool    _set;

        /* default value to be stored (only returned with a call
         * to '::value()' */
        OptionType          _default_type;
        void                *_default_ptr;

        /* location to store value upon '::set' */
        OptionType          _store_type;
        void                *_store_ptr;
        callback_slot_t     _store_cb;

        action_void_cb_t    _action_void_cb;
        action_set_void_t   _handler_void_cb;
        void                *_handler_void_ptr;

        callback_slot_t   _callback_signal;
};

typedef std::map<std::string,Option*> Option_map_t;
typedef Option_map_t::iterator Option_map_i;
typedef std::pair<Option_map_i,bool> Option_ins_t;
typedef std::pair<std::string,Option*> Option_pair_t;


class Args {

    public:

        Args();

        Option &addRequiredArgument( const char *key, char ch = '\0' );
        Option &addArgument( const char *key, char ch = '\0' );

        void parse_args( int argc, char *argv[] );

        bool defined( const char *key );

        const char * operator[]( const char *key );

        void help( void );
        void version( void );

        const char *basename( void );

        void desc( const char *fmt, ... );

    protected:

    private:

        Option_map_t   _options;

        char    *_basename;
        char    *_appname;

        char    *_desc;

};

extern Args    parser;

#endif
