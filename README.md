# What is Slurm Miner?

Slurm Miner extracts job data from Slurm using "sacct" and then injects into a PostgreSQL database.

# Why Use Slurm Miner?

Mining information about Slurm jobs using "sacct" is rather difficult, with no inherent advanced query support. While Slurm uses a MySQL database in the backend it is not recommended to query the database directly.

By loading Slurm job data into PostgresSQL it's possible to use SQL to learn more about what's happening in the clusters. Check out the example *SCHEMA* and *SQL* queries.

# How does it work?

Slurm Miner is designed to be idempotent, in that it can be run multiple times over the same data without affecting what has been stored in the PostgreSQL database.

Once the historical data has been loaded, it can be set run in a cron job and update the database as the job information changes. From cron it can run as often as desired, but just ensure that there are no gaps between the mining periods.

Examine 'miner.yaml' for a sample configuration.

# The SQL Schema

Examine "schema.sql" - this can be pre-loaded as is into an empty database, or adjust "miner.yaml" to match an existing target database.

And now some performance notes:

## Connecting to PostgreSQL

in *sminer.yaml* there is a configuration section. Either of the two valid PostgreSQL connection
types are supported:

Using _PQconnectdb_:


```
postgres:
   connect:     "host=<SERVER> dbname=<DATABASE> user=<USERNAME> password=<PASSWORD>"
```

Using _PQsetdbLogin_:


```
postgres:
    username:   "<POSTGRES USERNAME>"
    password:   "<POSTGRES PASSWORD>"
    database:   "<POSTGRES DATABASE>"
    server:     "<POSTGRES SERVER>"
```

## Table References

### Job

At a minimum the "job" table must be defined - this is the only mandatory table. Even though "sacct" merges both job and step output, some fields are only relevant to jobs or steps. Hence consider carefully which fields are being collected into the job table.

### Step

The "step" table is optional, but if defined will be used to store the step data

### Job Node Link

The "job_node_link" table provides a many-to-one (or many-to-many) mapping of nodes to jobs. Node names are expanded from the "nodelist" field, so each row will be an individual node-to-job mapping. If a node table exists specify the reference in the field YAML and node records will be created automatically.

### Step Node Link

The "step_node_link" table provides a many-to-one (or many-to-many) mapping of nodes to steps. Node names are expanded from the "nodelist" field, so each row will be an individual node-to-step mapping. If a node table exists specify the reference in the field YAML and node records will be created automatically.

### User, Node and Cluster

The following description is equally applicable to user, node and cluster tables.

Having each table with only a single column is probably not so helpful, and just adds complexity to schema. However in case the target schema does in fact have any of these tables already defined slurm-miner can connect to them.

*important* when defining these connected tables ensure that the schema lets slurm-miner create rows with a single column if records do not exist.

# How to Build #

It builds on CentOS 7 with the only dependencies being "yaml-cpp", "sigc++-2.0" and "postgresql-devel".

```
$ make
```

#  Configuration

Due to the miner.yaml file having the database credentials it's recommend that it is installed only readable by a special user or root.


## Parsing SACCT Data

At the bottom of the sample *sminer.yaml* is a section for "sacct":

```
sacct:

    # the following are only valid for jobs
    reservation:        String
    user:               String
    partition:          String
    qos:                String
    ...
```

This is where a parser is defined for each field type. "String" doesn't do any validation while
the other parsers do, each doing something a little different. The available parsers are:

- JobID - **mandatory** - required to understand the difference between jobs and steps
- TRES - used to split TRES (cpu=1,gres/gpu=2,...) into different values (see the table section below)
- DateTime - Used to interpret the Slurm DateTime format and translate it to Postgres's format
- Integer - Used to ensure the presented value is a whole number
- NodeList - Used to expand compacted node names (ie: cpu[1,2] -> cpu1,cpu2)
- State - Used to split the state code from a reason
- Bytes - Used to interpret ISO prefixes (ie: 'M', 'K' etc)
- String - No data interpretation

Additionally, the parser for "Integer" is special as it has a small RPN (Reverse Polish Notation)
engine that can be used to change the value before presenting to PostgreSQL. The example below will divide
the slurm output of "maxvmsize" by 1000, and then pass it to the PostgreSQL loader.

```
    maxvmsize:
        # MaxVMSize presents as KBytes, so convert to MB
        type:           Integer
        calc:           1000 /
```

## Connecting SACCT fields to tables

In each table definition there is a *fields* section, which is where the fields of the table
are defined.

```
tables:

    job:

        name:       slurm_job

        index:
            - job_id
            - time_submit

        fields:
            job_index:
                parser:         pkey
                type:           'INT'

            job_id:
                slurm:          jobid
                type:           'VARCHAR'

            tres_memory:
                slurm:      alloctres:memory
                calc:       1048576 /
                type:       'INT'

```

In the above "job" is a special keyword that denotes that this is the definition for the
job table, with the database table name specified in the "name" field.

The "index" section defines a unique index that references a particular row of a table.
While the primary key of the table is "job_index" when sminer is loading records it can only
search on what Slurm is presenting, so to load a specific Job record both "job_id" and "time_submit"
are used together.

Most field definitions are of the form:

```
        fields:
            job_id:
                slurm:      jobid
                type:       VARCHAR
```

Here the "slurm" field references one of the fields defined in the "sacct" section.
The type (above example VARCHAR) is the cast that is applied to the data when a record
is being inserted or updated.

Some tables have primary keys that are not assosicate with Slurm. These are best used when joining
tables together (so for example, when a step record connects to a job record, use the single column
primary key rather than using a compound key). Sminer knows that these values are not available from
slurm but are used when connecting tables.

```
        fields:
            job_index:
                parser:         pkey
                type:           'INT'
```


The sacct field with a type of "TRES" is special as this is used to split the individual
data sections into distinct values in prepartion for insertion into the database. The example
below shows how to reference the field "alloctres" and select the "memory" element. This
brings great power as it's possible to use SQL queries that natively access the individual elements
of a TRES but as a first class field.

Any label in a tres output (ie: cpu, memory,gres/gpu,billing,... can be used)

The 'calc' option for a TRES field is handled differently than how it is handled for a
natural sacct field. The 'calc' parameter for a TRES is specified in the table definition
section rather than in the sacct section. 'calc' is optional.

```
            tres_memory:
                slurm:      alloctres:memory
                type:       'BIGINT'
                calc:       1048576 /
```

If the tables are using referential integrity then sminer can ensure that these are maintained.
In the example below the field "job_index" references the "job_index" field of the table "slurm_job".
The "job_index" is automatically generated when a new record is inserted and is returned by the
insert operation, hence the step record can access the internal job_index value.

```
    fields:

        step_index:
            parser:         pkey
            type:           'INT'

        job_index:
            references:     slurm_job(job_index)
            type:           'INT'
```

Finally, the job and step node link tables exist to tie nodes to jobs. Both fields in this table
reference other fields. "Job_index" references the primary key of "slurm_job" and "node_name"
references the primary key of "slurm_node". When a "job_node_link" record is created if the
required node_name record doesn't exist in slurm_node then it will be automatically created.

If there's no "slurm_node" table then just remove the "references: .." and the check won't happen.

Why have separate node table? There can be value having more data associated with the node (ie: service date, serial number, etc...).

```
table:

    job_node_link:

        name:   slurm_job_node_link

        index:
            - job_index
            - node_name

        fields:
            job_index:
                references:     slurm_job(job_index)
                type:           'INT'

            node_name:
                references:     slurm_node(node_name)
                slurm:          nodelist
                type:           'VARCHAR'

```

## The /etc/sminer.yaml

The file /etc/sminer.yaml is used for the configuration data, however if the file is in a
different location then use "--config <pathname>" to specify a different location.

When sminer runs it can be quite chatty, and writes everything to the specified log file.

# The first run.

To load your existing slurm data use...

```
$ sminer --start YYYY-MM-DD
...
```
... and then wait. In testing it took about 24 hours to load 20M job records and 38M step records. Sminer usually loads in chunks of 24 hours, but this can be altered with the `--duration <days>`. Performance maybe higher when loading with longer duration but remember that all _known_ records are held in memory for a period, so memory usage will increase.

# Running periodically

To keep PostgreSQL up to date sminer can be run as frequently as desired. Runs are idempotent in that sminer will only make changes to the PostgreSQL database if it differs from the data presented by slurm. For example, when loading historical data there should not be any updates, however if jobs are still running then records maybe updated.

In testing the time to scan the previous 7 days in one chunk was faster than scanning the past 7 days one day at a time. (--scan 7 --duration 7 = 162 seconds, --scan 7 --duration 1 = 214 seconds). Remember thou that the '--duration 7' scan loaded all records from the database for the period before sacct is executed. Depending on the job velocity this maybe a considerable amount of memory required.

# Performance Optimisations

While the application has been built for speed, it can place some load on PostgreSQL. Worth noting is that if information from step records isn't required then don't include them in the definitions. Even though a field is defined in the "sacct" section it will only be included in the sacct query if has been mapped to a table.

# Bugs?

Yes. They exist. Please report them.

Feature requests? Will be considered.

END.
