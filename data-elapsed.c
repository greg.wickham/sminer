/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-elapsed.h"

#undef DEBUG

DataElapsed::DataElapsed( const char *field ) : Data( field, DATATYPE_ELAPSED ) {
    
}

void DataElapsed::_clear( void ) {

}

Data *DataElapsed::make( const char *field, const YAML::Node &config ) {
    return( new DataElapsed( field ) );
}

int DataElapsed::parse( const char *text, size_t len  ) {

    if (( len == 9 )&&( strncmp( text, "UNLIMITED", len ) == 0 )) {
        /* do nothing */
        return( 0 );
    }

    size_t olen = len;

    /*
     * Elapsed  time  fields  are  presented  as
     *
     *   [days-]hours:minutes:seconds[.microseconds].
     *
     *   Only 'CPU' fields will ever have microseconds.
     *
     * [from sacct manual]
     */

    const char *dp;

    if (( dp = xstrnchr( text, len, '.' )) != NULL ) {
        /* reduce the length of the string */
        len -= ( dp - text );
    }

    /* obtain 00:00 */

    if ( len < 5 ) {
        throw( AppWarning( "%s: elapsed is too short %ld\n", _field, len ) );
    }

    time_t seconds = 0;

    try {

        seconds =
            toNumber( &text[ len - 5 ], 2 ) * 60 +
            toNumber( &text[ len - 2 ], 2 );

    } catch ( int e ) {
        std::string param( text, olen );
        throw( AppWarning("%s: processing %s\n", _field, param.c_str() ) );
    }

    if ( text[ len - 3 ] != ':' ) {
        throw( AppWarning("%s: missing ':'", _field ) );
    }

    len -= 5;

    if ( len > 0 ) {

        if ( len < 3 ) {
            printf("Not enough data for hours\n");
        }

        if ( text[ len - 1 ] != ':' ) {
            throw( AppWarning("%s: missing hour ':' separator", _field ) );
        }

        seconds += toNumber( &text[ len - 3 ], 2 ) * 3600;

        len -= 3;

        if ( len > 0 ) {

            if ( text[ len - 1 ] != '-' ) {
                throw( AppWarning("%s: missing day '-' separator", _field ) );
            }

            size_t days = toNumber( text, len - 1 );

#if 0
            /* elapsed CPU can have hundreds of days (ie: 14 days x 1000 CPUs is 14000 days) */
            if ( days > 128 ) {
                std::string tmp( text, olen );
                throw( AppWarning("%s: calculated days '%d' is too big", _field, days, tmp.c_str() ) );
            }
#endif

            seconds += days * 86400;

        }

    }

    _value = std::to_string( seconds );
    _set = true;

    return( 0 );
}

/* END OF FILE */
