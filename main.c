/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "sacct.h"
#include    "config.h"
#include    "mylib.h"
#include    "args.h"
#include    "sacct.h"
#include    "app.h"
#include    "postgres.h"

Args    parser;

void version( const char *value );

int main( int argc, char *argv[] ) {

    SACCT   sacct( config );

    parser.desc("Slurm Miner - a tool for transferring \"sacct\" records into a PostgreSQL database");

    parser.addRequiredArgument( "config" )
        .value( "/etc/sminer.yaml" )
        .desc("Configuration file" );

    parser.addRequiredArgument( "duration", 'd' )
        .value( "1" )
        .call( &SACCT::setDayCount, &SACCT::duration )
        .desc("Length of period in days for records to be retrieved (format: integer)" );

    parser.addRequiredArgument( "start", 's' )
        .call( &SACCT::setTime, &SACCT::timeStart )
        .desc( "Timestamp for starting scan (format: YYYY-MM-DD)" );

    parser.addRequiredArgument( "end", 'e' )
        .call( &SACCT::setTime, &SACCT::timeEnd )
        .desc( "Timestamp for ending the scan (format: YYYY-MM-DD)" );

    parser.addRequiredArgument( "scan" )
        .call( &SACCT::setDayCount, &SACCT::scan )
        .desc( "Number of days preceding now to scan (format: integer)" );

    parser.addArgument( "running", 'r' )
        .value( false )
        .update( &app.saveRunning )
        .desc( "Save RUNNING job data" );

    parser.addRequiredArgument( "logfile", 'l' )
        .value( "/var/log/sminer.log" )
        .desc( "Log file" );

    parser.addArgument( "version", 'V' )
        .call( sigc::ptr_fun( &version ) )
        .desc( "Display version, copyright and license" );

    parser.addArgument( "verbose", 'v' )
        .value( false )
        .update( &app.verbose )
        .desc( "Display verbose messages" );

    parser.addRequiredArgument( "priority", 'p' )
        .desc( "Set the logging priority level" );

    parser.addArgument( "stdout" )
        .value( true )
        .update( &app.en_stdout )
        .desc( "Display messages on STDOUT");

    parser.addArgument( "updates", 'u' )
        .update( &app.updates )
        .desc( "Display updates to the database" );

    parser.addRequiredArgument( "state" )
        .desc("Write run state to this file"); 

    parser.addArgument( "quiet" )
        .desc( "Suppress all messages to stdout and stderr" );

    parser.addRequiredArgument( "debug" )
        .value( "0" )
        .update( sigc::mem_fun( app, &App::setDebug ) )
        .desc( "Enable debugging" );

    parser.addArgument( "nodelete" )
        .update( sigc::mem_fun( app, &App::setNoDelete ) )
        .desc( "Disable deleting of records" );

    parser.parse_args( argc, argv );

    const char *filename = parser["config"];

    try {
        config.load( filename );
    } catch ( AppExcept &e ) {
        app.error( "%s: %s\n", filename, e.what() );
        app.exit( 1 );
    } catch ( AppFatal &e ) {
        app.error( "%s: %s\n", filename, e.what() );
        app.exit( 2 );
    }

    /* with the log file loaded, now set any app parameters */

    try {
        app.configure( parser );
    } catch ( AppFatal &e ) {
        app.error( "config error: %s\n", e.what() );
        app.exit( 2 );
    } 

    /* check if --stdout and --quiet have both been defined */

    if ( parser.defined("stdout") && parser.defined("quiet") ) {
        app.error( "%s: %s", app.program, "Do not define --quiet and --stdout");
        app.exit( 4 );
    }

    /* from now on use the logging system */

    try {
        pg.connect();
        sacct.run();
    } catch ( AppFatal &e ) {
        app.error( "%s: %s", app.program, e.what() );
        app.exit( 3 );
    }

    app.summary();

    app.exit( 0 );
}

void version( const char *value ) {

    printf("%s: %s\n", PROG_NAME, PROG_VERSION );
    printf("Author: %s\n", PROG_AUTHOR );
    printf("Built: %s\n", PROG_BUILD );
    if ( PROG_COPYRIGHT ) {
        printf("Copyright: %s\n", PROG_COPYRIGHT );
    }
    if ( PROG_LICENSE ) {
        printf("License: %s\n", PROG_LICENSE );
    }
    if ( PROG_GIT ) {
        printf("Git Repo: %s\n", PROG_GIT );
    }

    exit( 0 );
}

/* END OF FILE */
