/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _TIMER_H_
#define _TIMER_H_

#include    "mylib.h"

class Timer {

    public:

        Timer();

        void start( void );
        void stop( void );
        bool running( void );

        time_t  sec( void );
        time_t  usec( void );

        double  seconds( void );

    private:

        bool            _running;
        struct timeval  _begin;
        struct timeval  _end;
        struct timeval  _diff;

};

#endif

