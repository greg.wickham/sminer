/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-node.h"

/* a simple store for keeping track of all nodes */
Node_map_t DataNode::nodes;

DataNode::DataNode( const char *field ) : Data( field, DATATYPE_NODE ) {
    
}

Data *DataNode::make( const char *field, const YAML::Node &config ) {
    return( new DataNode( field ) );
}

void DataNode::_clear( void ) {
}

int DataNode::parse( const char *text, size_t len ) {

    std::string name( text, len );

    Node_map_i i;

    if (( i = nodes.find( name )) == nodes.end() ) {
        nodes.insert( std::pair<std::string,Node>( name, Node() ));
    }

    return( 0 );
}

/* END OF FILE */
