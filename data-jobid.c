/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-jobid.h"

DataJobID::DataJobID( const char *field ) : Data( field, DATATYPE_JOBID ) {
    
}

Data *DataJobID::make( const char *field, const YAML::Node &config ) {
    return( new DataJobID( field ) );
}

void DataJobID::_clear( void ) {
    _step = false;
    _job.clear();
}

int DataJobID::parse( const char *text, size_t len ) {

    _value.assign( text, len );
    _set = true;

    _step = _value.find(".");

    if ( _step > 0 ) {
        _job.assign( _value.c_str(), _step );
    } else {
        _job.clear();
    }

    return( 0 );
}

const char *DataJobID::job( void ) {
    return( _job.size() > 0 ? _job.c_str() : NULL );
}

/* END OF FILE */
