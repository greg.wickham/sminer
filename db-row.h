/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _DB_ROW_H_
#define _DB_ROW_H_

#include    "mylib.h"
#include    "mytypes.h"
#include    "db-field.h"

namespace DB {

    class Table;
    class Row;

    typedef std::map<std::string,Row*> Row_map_t;
    typedef Row_map_t::iterator Row_map_i;
    typedef std::pair<std::string,Row*> Row_pair_t;
    typedef std::pair<Row_map_i,bool> Row_ins_t;

    class Row {

        public:

            /* constructor */
            Row( Table *table, findex_t nfields );
            ~Row();

            /* set a value; dirty is cleared */
            void set( findex_t index, const char *text );

            /* update a value; dirty is set if a change happened */
            void update( findex_t index, const char *text );

            /* fetch a value from the row */
            const char *fetch( findex_t field );

            /* return true if any field is different */
            bool dirty( void );

            /* return true if a specific field is different */
            bool dirty( findex_t field );

            /* clear dirty flags */
            void sync( void );

            /* is this record maintained */
            bool used( void );

            /* is a value set for this field? */
            bool isset( findex_t field );

            /* dump a row */
            void dump( void );
            void dump( const char *tablename, DB::Field **fields );

            /* drop this row from the DB */
            void drop( void );

            void parent( Row *ptr );
            Row *parent( void );

        private:

            Table   *_table;

            /* parent record of SQL relationship */
            Row     *_parent;

            findex_t  _nfields;

            /* values - an array of values */
            char    **_values;

            /* dirty flag - indicates if value
             * is different than what is stored in the
             * database */
            bool    *_dirty;

            /* set flag - is a value valid
             */
            bool    *_set;

            /* set to true if the data is 'ensured' during the processing */
            bool    _used;

    };

};

#endif
