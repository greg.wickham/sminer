/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _STATS_H_
#define _STATS_H_

class Stats {

    public:

        Stats( const char *name );
        void dump( void );

        void clear( void ) {
            insert = 0;
            duplicate = 0;
            update = 0;
            clean = 0;
            error = 0;
            select = 0;
            discard = 0;
            deleted = 0;
            would_delete = 0;
        };

        int     insert;
        int     duplicate;
        int     update;
        int     clean;
        int     error;
        int     select;
        int     discard;
        int     deleted;
        int     would_delete;

    private:

        char    *_name;

};

extern Stats table_node;
extern Stats table_job;
extern Stats table_job_link;
extern Stats table_step;
extern Stats table_step_link;
extern Stats table_user;

#endif
